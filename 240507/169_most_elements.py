def most_elements(nums):
    count = len(nums) // 2
    d = {}
    for i in nums:
        if i in d:
            d[i] += 1
        else:
            d[i] = 1
    m = [0, 0]
    for k, v in d.items():
        if v > count and v > m[1]:
            m = (k, v)
    return m[0]


def most_elements2(nums):
    nums.sort()
    return nums[len(nums) // 2]


def most_elements3(nums):
    """
    只适合2个数的列表，一样就+1， 不一样-1 遇到0换数字，就能得到最多的
    """
    count = 0
    candidate = None

    for num in nums:
        if count == 0:
            candidate = num
        count += (1 if num == candidate else -1)

    return candidate


if __name__ == '__main__':
    a = [2, 2, 2, 3, 3, 1, 1]
    print(most_elements3(a))
