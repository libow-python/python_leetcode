from typing import List


def remove_duplicates(nums: List[int]) -> int:
    """
    思路：
    递增
    1 使用双指针解决问题
    2 因为是递增所以只要第一次两个指针相等那么就是满足条件的, 不管怎么样都要将慢指针当前的下一位赋值为快指针
    3 下次遇见相等只移动快指针
    4 不相等的值将慢指针的下一个值赋值快指针 如果慢指针指第一位 那么就给相邻的下一位赋值为快指针， 如果是第二位，只有遇见不相等的值的时候才会给下一位赋值

    也就是说，相等的时候我会给第二位赋值 也就是让相邻的两个数保持一直，不相等的时候我从下一位开始变成新的值
    """
    nums_len = len(nums)
    if nums_len <= 1:
        return len(nums)
    slow, fast = 0, 1
    tag = False
    while fast < nums_len:
        if nums[slow] == nums[fast]:
            if not tag:
                tag = True
                slow += 1
                nums[slow] = nums[fast]
            fast += 1
        else:
            slow += 1
            nums[slow] = nums[fast]
            tag = False
            fast += 1
    print(nums)
    return slow + 1


def remove_duplicates_v2(nums: List[int]) -> int:
    """
    因为是有序
    本题要求相同元素最多出现两次而非一次，所以我们需要检查上上个应该被保留的元素
    i ： 需要更改的位置 需要插入的位置
    k : 遍历全部的数组，将k位置的数和需要插入的i - 2的位置的数比较，如果不一致就替换 因为有序
    """
    i = min(2, len(nums))
    for k in range(2, len(nums)):
        if nums[k] != nums[i - 2]:
            nums[i] = nums[k]
            i += 1
    print(nums)
    return i


if __name__ == '__main__':
    ns = [1, 1, 1, 2, 2, 3, 4]
    print(remove_duplicates_v2(ns))
