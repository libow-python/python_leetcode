from typing import List


def remove_value(nums: List[int], val: int) -> int:
    while val in nums:
        nums.remove(val)
    return len(nums)


def remove_value2(nums: List[int], val: int) -> int:
    """
    实际上就是个双指针移动
    """
    max_len = len(nums)
    p1 = 0
    for i in range(max_len):
        if nums[i] != val:
            nums[p1] = nums[i]
            p1 += 1
    return len(nums[:p1])


def remove_value3(nums: List[int], val: int) -> int:
    """
        双指针移动
    """
    p1, p2 = 0, 0
    while p2 < len(nums):
        if nums[p2] != val:
            nums[p1] = nums[p2]
            p1 += 1
        p2 += 1
    return p1


if __name__ == '__main__':
    n = [0, 1, 2, 2, 3, 0, 4, 2]
    v = 3
    print(remove_value3(n, v))
