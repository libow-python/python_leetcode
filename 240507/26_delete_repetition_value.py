from typing import List


def remove_duplicates(nums):
    n_l = len(nums)
    p1, p2 = 0, 0
    dup_lst = []
    while p2 < n_l:
        if nums[p2] not in dup_lst:
            dup_lst.append(nums[p2])
            nums[p1] = nums[p2]
            p1 += 1
        p2 += 1
    print(nums[:p1])
    return p1


def remove_duplicates2(nums):
    """
    优化
    """
    n_l = len(nums)
    p1, p2 = 0, 0
    dup_lst = []
    while p2 < n_l:
        if nums[p2] not in dup_lst:
            dup_lst.append(nums[p2])
            nums[p1] = nums[p2]
            p1 += 1
        p2 += 1
    print(nums[:p1])
    return p1


def remove_duplicates3(nums: List[int]) -> int:
    """
    递增的情况下
    """
    slow, fast = 0, 1
    while fast < len(nums):
        if nums[fast] != nums[slow]:
            slow = slow + 1
            nums[slow] = nums[fast]
        fast = fast + 1
    print(nums)
    return slow + 1


if __name__ == '__main__':
    n = [1, 1, 1, 2, 2, 3]
    print(remove_duplicates3(n))
