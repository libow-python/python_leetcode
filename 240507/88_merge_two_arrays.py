def merge(arr1, m, arr2, n):
    """
    库函数
    """
    for i in range(n):
        arr1[m+i] = arr2[i]
    arr1.sort()
    return arr1


def merge2(arr1, m, arr2, n):
    """
    双指针
    适用于单调递增
    """
    res = []
    p1, p2 = 0, 0
    while p1 < m or p2 < n:
        if p1 == m:
            res.append(arr2[p2])
            p2 += 1
        elif p2 == n:
            res.append(arr1[p1])
            p1 += 1
        elif arr1[p1] < arr2[p2]:
            res.append(arr1[p1])
            p1 += 1
        elif arr1[p1] >= arr2[p2]:
            res.append(arr2[p2])
            p2 += 1
    return res


def merge3(arr1, m, arr2, n):
    """
    逆向双指针
    arr1是有多余位置站位的
    """
    p1, p2 = 0, 0
    # 需要添加的位置
    idx = m + n - 1
    while p1 < m or p2 < n:
        if p1 == m:
            arr1[idx] = arr2[n - p2 - 1]
            p2 += 1
        elif p2 == n:
            arr1[idx] = arr1[m - p1 - 1]
            p1 += 1
        elif arr1[m - p1 - 1] <= arr2[n - p2 - 1]:
            arr1[idx] = arr2[n - p2 - 1]
            p2 += 1
        elif arr1[m - p1 - 1] > arr2[n - p2 - 1]:
            arr1[idx] = arr1[m - p1 - 1]
            p1 += 1

        idx -= 1
    print(arr1)
    return arr1


if __name__ == '__main__':
    nums1 = [1, 2, 3, 0, 0, 0]
    m1 = 3
    nums2 = [2, 5, 6]
    n1 = 3
    # print(merge(nums1, m1, nums2, n1))
    print(merge3(nums1, m1, nums2, n1))
