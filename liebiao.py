# 实现一个链表

class Node():
    def __init__(self, item):
        self.item = item
        self.next = None

class SingleLinkList():
    """单链表"""

    def __init__(self, node):
        self._head = node

    def is_empty(self):
        """链表是否为空"""
        # 根据head变量的值来决定返回值
        return self._head == None

    def length(self):
        # 链表的长度
        # cur游标，用来移动遍历节点
        cur = self._head
        # count 用来记录数量
        count = 0
        # 若为cur.next != None不进最后一次循环体
        while cur != None:
            count += 1
            # 现在拿到当前的游标的next属性，这个属性存放的是下一个元素
            # 实质上是将下一个元素的地址存放在元素的属性中，寻址是python自己完成的
            cur = cur.next
        return count

    def travel(self):
        # 遍历链表
        cur = self._head
        # while cur != None:
        while cur:
            print("当前的cur的elem : %d" % cur.elem)
            cur = cur.next

    def append(self, item):
        """
        # 链表尾部添加元素 ，尾插法
        # item 指的是数据而不是节点（对于用户而言）
        # 以下代码没有考虑第一个是怎么添加的
        cur = self._head
        while cur != None:
            if cur.next == None:
                item_obj = Node(item)
                cur.next = item_obj
                break
            cur = cur.next
        """
        item_obj = Node(item)
        # 要判断一下当前的链表是否为空
        cur = self._head
        if self.is_empty():
            self._head = item_obj
        else:
            while cur.next != None:
                cur = cur.next
            # 目前cur来说就是最后一个
            cur.next = item_obj

    def add(self, item):
        # 链表头部添加元素，头插入
        """
        思路：首先要将添加进来的首节点和现有的节点连接在一起才能保证数据
              的不丢失
        :param item:
        :return:
        """
        node = Node(item)
        node.next = self._head
        self._head = node


    def insert(self, pos, item):
        # 指定的位置添加元素
        '''

        :param pos: 从0 开始的
        :param item:
        :return:
        '''
        # 还要考虑如果要求插入的位置大于长度就是尾插
        cur = self._head
        count = 0
        node = Node(item)
        if pos <= 0:
            node.next = self._head
            self._head = node
        # 在尾部插入
        elif pos > self.length()-1:
            self.append(item)
        else:
            while cur != None:
                if count == pos - 1:
                    cur_next = cur.next
                    cur.next = node
                    node.next = cur_next
                    break
                count += 1
                cur = cur.next

        # 哔哩哔哩代码
        # pre = self._head
        # count = 0
        # node = Node(item)
        # if pos <= 0:
        #     node.next = self._head
        #     self._head = node
        #     self.add(item)
        # 在尾部插入
        #     elif pos > self.length()-1:
        #         self.append(item)
        # while count<(pos-1):
        #     pre = pre.next
        #     count += 1
        # # 当循环退出后走到现在就是你要插入的位置，现在就是要插入的位置 的前一个位置
        # node.next = pre.next
        # pre.next = node

        def remove(self, item):
            # 删除节点
            # 可以设置两个游标
            # cur =  pre.next
            cur = self._head
            # 如果是第一个就把头去掉就可以
            if cur.elem == item:
                self._head = cur.next
            else:
                while cur != None:
                    cur_next = cur.next
                    # 以下语句是为防止cur_next.elem报错
                    if cur_next == None:
                        break
                    if cur_next.elem == item:
                        cur.next = cur_next.next
                        break
                    cur = cur.next

            def search(self, item):
                # 查找节点是否存在
                cur = self._head
                while cur != None:
                    if cur.elem == item:
                        return "存在该节点"
                    cur = cur.next
                return "不存在该节点"
