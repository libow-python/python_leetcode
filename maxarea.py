def maxArea(height) -> int:
    left = 0
    right= len(height) - 1
    max_area = 0
    while left < right:
        if height[left] < height[right]:
            area = height[left] * (right-left)
            max_area = max(area, max_area)
            left += 1
        elif height[left] >= height[right]:
            area = height[right] * (right-left)
            max_area = max(area, max_area)
            right -= 1
    return max_area


if __name__ == '__main__':
    print(maxArea([1, 8, 6, 2, 5, 4, 8, 3, 7]))
    print(maxArea([1,2,1]))