# 迷宫问题 回溯，栈，出栈，深度优先

maze = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 1, 0, 0, 0, 1, 0, 1],
    [1, 0, 0, 1, 0, 0, 0, 1, 0, 1],
    [1, 0, 0, 0, 0, 1, 1, 0, 0, 1],
    [1, 0, 1, 1, 1, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 1, 0, 0, 0, 0, 1],
    [1, 0, 1, 0, 0, 0, 1, 0, 0, 1],
    [1, 0, 1, 1, 1, 0, 1, 1, 0, 1],
    [1, 1, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
]

directions = [
    lambda x, y: (x + 1, y),
    lambda x, y: (x, y + 1),
    lambda x, y: (x, y - 1),
    lambda x, y: (x - 1, y),
]


def maze_path(maze, x1, y1, x2, y2):
    pass
    stack = []
    stack.append((x1, y1))
    while (len(stack) > 0):
        cur_node = stack[-1]
        if cur_node[0] == x2 and cur_node[1] == y2:
            for i in stack:
                print(i)
            return True
        for dirs in directions:
            next_node = dirs(cur_node[0], cur_node[1])
            if maze[next_node[0]][next_node[1]] == 0:
                stack.append(next_node)
                maze[next_node[0]][next_node[1]] = 2
                break
        else:
            maze[cur_node[0]][cur_node[1]] = 2
            stack.pop()
    else:
        print("没有路 ...")
        return False
dirsone = [
    lambda x,y : (x+1,y),
    lambda x,y : (x-1,y),
    lambda x,y : (x,y+1),
    lambda x,y : (x,y-1),
]

def scond_maze_path(maze, x1, y1, x2, y2):
    stack = []
    stack.append((x1, y1))
    maze[x1][y2] = 2
    while (len(stack) > 0):
        cur_node = stack[-1]
        if cur_node[0] == x2 and cur_node[1] == y2:
            return stack
        for d in dirsone:
            next_node = d(cur_node[0],cur_node[1])
            if maze[next_node[0]][next_node[1]] == 0:
                stack.append(next_node)
                maze[next_node[0]][next_node[1]] = 2
                break
        else:
            maze[cur_node[0]][cur_node[1]] = 2
            stack.pop()
    else:
        print("没有路....")
        return stack


if __name__ == '__main__':
    c = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 1, 0, 0, 0, 1, 0, 1],
    [1, 0, 0, 1, 0, 0, 0, 1, 0, 1],
    [1, 0, 0, 0, 0, 1, 1, 0, 0, 1],
    [1, 0, 1, 1, 1, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 1, 0, 0, 0, 0, 1],
    [1, 0, 1, 0, 0, 0, 1, 0, 0, 1],
    [1, 0, 1, 1, 1, 0, 1, 1, 0, 1],
    [1, 1, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
]
    print(maze_path(maze, 1, 1, 8, 8))
    print(scond_maze_path(c, 1, 1, 8, 8))
