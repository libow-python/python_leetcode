from collections import deque
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def hasPathSum(self, root, targetSum: int) -> bool:
        """广度优先遍历"""
        if not root: return False
        queue = deque([root])
        val_queue = deque([root.val])
        res = []
        while queue:
            node = queue.pop()
            node_val = val_queue.pop()
            if not node.left and not node.right:
                res.append(node_val)
                continue
            if node.left:
                queue.append(node.left)
                val_queue.append(node_val+node.left.val)
            if node.right:
                queue.append(node.right)
                val_queue.append(node_val+node.right.val)
        return targetSum in res
    
    def hasPathSum2(self, root, targetSum: int) -> bool:
        """深度优先"""
        if not root: return False
        def cal_node(rt):
            if not rt.left and not rt.right:
                return [rt.val]
            elif rt.left and not rt.right:
                return [i + rt.val for i in cal_node(rt.left)]
            elif rt.right and not rt.left:
                return [i + rt.val for i in cal_node(rt.right)]
            else:
                return [i + rt.val for i in cal_node(rt.right)] + [i + rt.val for i in cal_node(rt.left)]
        return targetSum in cal_node(root)
    
    def hasPathSum3(self, root, targetSum: int) -> bool:
        """深度优先优化?"""
        if not root: return False
        if not root.left and not root.right and root.val == targetSum:
            # 叶子结点上val = target
            return True
        return self.hasPathSum3(root.left, targetSum - root.val) or self.hasPathSum3(root.right, targetSum - root.val)

        

if __name__ == '__main__':
    n1 = TreeNode(7)
    n2 = TreeNode(6)
    n3 = TreeNode(5)
    n4 = TreeNode(2)
    n5 = TreeNode(1)
    n6 = TreeNode(1)
    n5.right = n6
    n3.left = n5
    n2.right = n4
    n1.left = n2
    n1.right = n3
    print(Solution().hasPathSum3(n1, 13))