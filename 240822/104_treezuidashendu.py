# Definition for a binary tree node.
import collections
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def maxDepth(self, root) -> int:
        """
        深度优先策略
        思路，一个树节点的深度 = max(左子树的深度， 右子树的深度) + 1, 然后递归
        """
        if not root:
            return 0
        return max(self.maxDepth(root.left), self.maxDepth(root.right)) + 1
    
    def maxDepth2(self, root) -> int:
        """
        广度优先策略
        思路，每次记录一层的的节点，拓展一层（当前层不能全为None） + 1
        """
        if not root:
            return 0
        if not root.left and not root.right: return 1
        ans = 1
        queue = collections.deque([root.left, root.right])
        child_queue = collections.deque([])
        while queue:
            tree_node = queue.popleft()
            if tree_node:
                if tree_node.left:
                    child_queue.append(tree_node.left)
                if tree_node.right:
                    child_queue.append(tree_node.right)
            if not queue:
                queue = child_queue
                child_queue = collections.deque([])
                ans += 1
        return ans
                

if __name__ == '__main__':
    n1 = TreeNode(1)
    n2 = TreeNode(2)
    n3 = TreeNode(3)
    n4 = TreeNode(4)
    n2.left = n4
    n1.left = n2
    n1.right = n3
    print(Solution().maxDepth2(n1))