class Solution:
    def searchInsert(self, nums: list[int], target: int) -> int:
        if not list: return 0;
        left, right = 0, len(nums) - 1
        if target <= nums[left]:
            return 0
        if target == nums[right]:
            return len(nums)-1
        if target >= nums[right]:
            return len(nums)
        while left + 1 <= right:
            if left + 1 == right:
                if nums[left] < target:
                    return left + 1
                else:
                    return left - 1
            middle = (left + right) // 2
            if nums[middle] == target:
                return middle
            if nums[middle] > target:
                right = middle
            if nums[middle] < target:
                left  = middle
        return 0

    def searchInsert2(self, nums: list[int], target: int) -> int:
        """标准二分"""
        left, right = 0, len(nums) - 1
        while left <= right:
            middle = (left + right) // 2
            if nums[middle] > target:
                right -= 1
            if nums[middle] < target:
                left += 1
            if nums[middle] == target:
                return middle
        return left
        


if __name__ == '__main__':
    lst = [1, 3, 6, 7]
    number = 9
    print(Solution().searchInsert2(lst, number))
