from collections import Counter
class Solution:
    def singleNumber(self, nums) -> int:
        c = Counter(nums)
        for k, v in c.items():
            if v == 1:
                return k
            
    def singleNumber1(self, nums) -> int:
        """
        只使用常量额外空间
        当算法所需的额外空间与输入规模无关,即不随输入数据的大小变化而变化时,我们称之为常量额外空间,其空间复杂度为O(1)
        使用位运算
        """
        res = 0
        for i in nums:
            res = res ^ i
        return res


if __name__ == "__main__":
    print(Solution().singleNumber1([2, 2, 1, 1, 4]))