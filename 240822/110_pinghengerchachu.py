"""
# 平衡二叉树的定义：
1. 每个子树都是平衡二叉树
2. 左右子树的深度差值绝对值不大于1
平衡因子(Balance Factor) ：二叉树上结点的左子树的深度减去其右子树深度称为该结点的平衡因子。
所以平衡因子的值只能是 -1, 0 , 1
最小二叉平衡树的节点的公式如下: F(n)=F(n-1)+F(n-2)+1
f(n-1) 左子树的节点数量
f(n-2) 右子树的节点数量
"""

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:

    def calculate_factor(self, root):
        """
        计算深度
        """
        if not root:
            return 0
        return max(self.calculate_factor(root.left),self.calculate_factor(root.right)) + 1

    def isBalanced(self, root) -> bool:
        """
        1. 平衡因子绝对值不大于1
        2. 左子树和右子树必须也是平衡树

        # 弊端是一个节点的深度会重复计算
        """
        if not root:
            return True
        left = root.left
        right = root.right
        left_factor = self.calculate_factor(left)
        right_factor = self.calculate_factor(right)
        if abs(left_factor - right_factor) > 1:
            return False
        return self.isBalanced(left) and self.isBalanced(right)
    
    def isBalanced2(self, root) -> bool:
        """自低向上递归
        自底向上递归的做法类似于后序遍历，
        重点在于后序遍历的思想
        对于当前遍历到的节点，
        先递归地判断其左右子树是否平衡，
        再判断以当前节点为根的子树是否平衡。
        如果一棵子树是平衡的，则返回其高度（高度一定是非负整数），
        否则返回 -1。如果存在一棵子树不平衡, 则整个二叉树一定不平衡
        """
        if not root: return True
        def deep(rt):
            if not rt:
                return 0
            l = rt.left
            r = rt.right
            l_d = deep(l)
            r_d = deep(r)
            if l_d == -1 or r_d == -1 or abs(l_d - r_d) > 1:
                return -1
            return max(l_d, r_d) + 1
        return deep(root) != 0


if __name__ == '__main__':
    n1 = TreeNode(1)
    n2 = TreeNode(2)
    n3 = TreeNode(3)
    n4 = TreeNode(4)
    n5 = TreeNode(5)
    # n4.left = n5
    # n2.left = n4
    n1.left = n2
    # n1.right = n3
    res = Solution().isBalanced2(n1)
    print(res)