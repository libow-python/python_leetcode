class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        return len(s.strip().split(" ")[-1])
    
    def lengthOfLastWord2(self, s: str) -> int:
        res = [0]
        l = 0
        for i in s:
            if i != ' ':
                l += 1
                if i == s[-1] and l:
                    res.append(l)
            else:
                if l:
                    res.append(l)
                l = 0
        return res[-1]
    
    def lengthOfLastWord3(self, s: str) -> int:
        count = 0
        local_count = 0

        for i in range(len(s)):
            if s[i] != ' ':
                local_count += 1
                count = local_count
            else:
                local_count = 0
        
        return count
    
if __name__ == '__main__':
    print(Solution().lengthOfLastWord3("Hello World  w  "))