import math
class Solution:

    def mySqrt(self, x: int) -> int:
        """
        难以划分精度
        """
        if x == 0: return 0
        left, right = 0, x
        jd = max(round(x / 1000000, 3), 0.1)
        while True:
            print(left, right)
            mid = round((left + right) / 2, 3)
            r = round(mid * mid, 3)
            print("r", mid, r)
            if r > x and r - x < jd:
                return int(mid)
            if r < x and x -r < jd:
                return int(mid)
            if r > x:
                right = mid
            elif r < x:
                left = mid
            else:
                return mid
            
    def mySqrt2(self, x: int) -> int:
        """
        二分查找只要左右都命中就停下，计算整数部位
        """
        l, r, ans = 0, x, -1
        while l <= r:
            mid = (l + r) // 2
            if mid * mid <= x:
                ans = mid
                l = mid+1
            else:
                r = mid-1
        return ans
    
    def mySqrt3(self, x: int) -> int:
        """
        使用e ln

        """
        if x == 0: return 0
        ans = int(math.exp(0.5*math.log(x)))
        return ans
    
    def mySqrt4(self, x: int) -> int:
        """牛顿迭代"""
        pass
            

if __name__ == '__main__':
    print(Solution().mySqrt3(2147483648), "2")