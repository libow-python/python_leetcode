# Definition for a binary tree node.
import collections
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:

    def isSame(self, lf, rh):
        if not lf and not rh:return False
        if lf.val == rh.val:
            lr, rr = False, False
            if lf.left and rh.right:
                lr = self.isSame(lf.left, rh.right)
                print(lf.left.val, rh.right.val, lr)
            elif not (lf.left or rh.right):
                lr = True
            else:
                return False
            if lf.right and rh.left:
                rr = self.isSame(lf.right, rh.left)
                print( lf.right.val, rh.left.val, rr)
            elif not (lf.right or rh.left):
                rr = True
            else:
                return False
            print("res:", lf.val, rh.val, lr, rr,  lf.left, lf.right, rh.left, rh.right)
            return lr and rr
        else:
            return False

    def isSymmetric(self, root) -> bool:
        """递归的算法"""
        if root.left is None and root.right is None: return True
        return self.isSame(root.left, root.right)
    
    def isSymmetric2(self, root):
        """迭代的算法 使用队列"""
        if not root: return root
        stack = [root.left, root.right]
        while stack:
            l = stack.pop()
            r = stack.pop()
            if l.val != r.val:
                return False
            if l.left and r.right:
                stack.append(l.left)
                stack.append(r.right)
            elif not l.left and not r.right:
                continue
            else:
                return False
            if l.right and r.left:
                stack.append(l.right)
                stack.append(r.left)
            elif not l.right and not r.left:
                continue
            else:
                return False
        return  True
    
    def isSymmetric3(self, root) -> bool:
        """迭代算法2"""
        if not root: return root
        queue = collections.deque([root.left, root.right])
        while queue:
            a_tree = queue.popleft()
            b_tree = queue.popleft()
            if a_tree is None and b_tree is None:
                continue
            elif a_tree and b_tree:
                if a_tree.val != b_tree.val:
                    return False
                queue.append(a_tree.left)
                queue.append(b_tree.right)
                queue.append(a_tree.right)
                queue.append(b_tree.left)
            else:
                return False
        return True


if __name__ == "__main__":
    n1 = TreeNode(1)
    n2 = TreeNode(2)
    n3 = TreeNode(3)
    n4 = TreeNode(4)
    n5 = TreeNode(2)
    n6 = TreeNode(3)
    n7 = TreeNode(4)

    n2.left = n3
    n2.right = n4
    n5.left = n7
    n5.right = n6
    n1.left = n2
    n1.right = n5

    print(Solution().isSymmetric3(n1))

    # n11 = TreeNode(1)
    # n12 = TreeNode(2)
    # n13 = TreeNode(3)
    # n14 = TreeNode(4)
    # n15 = TreeNode(2)
    # n16 = TreeNode(3)
    # n17 = TreeNode(4)

    # n2.left = n3
    # n2.right = n4
    # n5.left = n6
    # n5.right = n7
    # n1.left = n2
    # n1.right = n5
