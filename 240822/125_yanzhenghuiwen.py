import string
class Solution:
    def isPalindrome(self, s: str) -> bool:
        all_str = string.ascii_letters + string.digits
        res_str = ''
        for i in s:
            if i in all_str:
                res_str += i.lower()
        print(res_str, res_str[::-1])
        return res_str == res_str[::-1]
    
    def isPalindrome1(self, s: str) -> bool:
        new_str = [i.lower() for i in s if i.isalnum()]
        print(new_str, "new_str")
        left, right = 0, len(new_str) - 1
        res = True
        while left <= right:
            print(new_str[left], new_str[right])
            if new_str[left] != new_str[right]:
                res = False
                break
            left += 1
            right -= 1
        return res


if __name__ == "__main__":
    print(Solution().isPalindrome1("A man, a plan, a canal: Panama"))