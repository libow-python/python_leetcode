class Solution:
    def plusOne(self, digits: list[int]) -> list[int]:
        return [int(i) for i in list(str(int("".join([str(i) for i in digits])) + 1))]
    
    def plusOne2(self, digits: list[int]) -> list[int]:
        number = 0
        for indx, num in enumerate(digits[::-1]):
            number += (10 ** indx * num)
        print(number)
        number += 1
        return [int(i) for i in str(number)]


if __name__ == '__main__':
    print(Solution().plusOne2([1,2,3]))