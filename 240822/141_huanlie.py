class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def hasCycle(self, head) -> bool:
        f, s = head, head
        while f and s:
            s_next = s.next
            f_n = f.next
            if f.next:
                f_next = f_n.next
                if f_next and f_next.val == s_next.val:
                    return True
            else:
                f_next = f_n
            f = f_next
            s = s.next
        return False
    
    def hasCycle1(self, head) -> bool:
        """
        快慢指针
        """
        f, s = head, head
        while f and s:
            s = s.next
            f = f.next
            if f:
                f = f.next
                if f and f.val == s.val:
                    return True
        return False
    
    def hasCycle2(self, head) -> bool:
        """
        哈希表
        """
        res_set = set()
        while head:
            if head in res_set:
                return True
            res_set.add(head)
            head = head.next
        return False
    

 
if __name__ == "__main__":
    h1 = ListNode(3)
    h2 = ListNode(2)
    # h3 = ListNode(0)
    # h4 = ListNode(-4)
    h1.next = h2
    # h2.next = h3
    # h3.next = h4
    # h4.next = h2
    print(Solution().hasCycle2(h1))