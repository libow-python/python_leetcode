# 怎么考虑去判断是一对括号？ 使用栈去存储左括号
class Solution:
    def isValid(self, s: str) -> bool:
        k_map = {"(": ")", "{":"}", "[":']', "<":">"}
        k_stack = []
        if len(s) % 2 == 1: return False;
        for i in s:
            if i in k_map:
                k_stack.append(i)
            elif k_stack and k_map.get(k_stack[-1]) == i:   
                k_stack.pop(-1)
            else:
                return False
        print(k_stack)
        return not k_stack
    
    def isValid2(self, s: str):
        pass

if __name__ == '__main__':
    ss = "([}}])"
    print(Solution().isValid(ss))