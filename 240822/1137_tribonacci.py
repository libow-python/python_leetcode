def tribonacci(n: int) -> int:
    res_list = [0 for _ in range(n+1)]
    res_list[0], res_list[1], res_list[2] = 0, 1, 1
    for i in range(3, n+1):
        res_list[i] = res_list[i-1] + res_list[i-2] + res_list[i-3]
    return res_list[n]


if __name__ == '__main__':
    res = tribonacci(25)
    print(res)
