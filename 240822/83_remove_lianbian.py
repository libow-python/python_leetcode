class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
    
class Solution:
    def deleteDuplicates(self, head):
        if not head: return head
        cur = head
        while cur.next:
            cur_value = cur.val
            next_value = cur.next.val
            if cur_value == next_value:
                cur.next = cur.next.next
            else:
                cur = cur.next
        return head


if __name__ == '__main__':
    head = ListNode(1, ListNode(1, ListNode(1, ListNode(2, ListNode(2, ListNode(3))))))
    res = Solution().deleteDuplicates(head)
    print(res.val)
    print(res.next.val)
    print(res.next.next.val)
    print(res.next.next.next)
