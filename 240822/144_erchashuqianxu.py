# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution(object):

    def __init__(self):
        self.res = []
    
    def pre(self, root):
        if not root: return []
        self.res.append(root.val)
        self.preorderTraversal(root.left)
        self.preorderTraversal(root.right)

    def preorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        self.pre(root)
        return self.res
        
    def preorderTraversal1(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        也可以在这个函数上直接返回，在中间递归的途中，不适用返回值
        """
        if not root: return []
        self.res.append(root.val)
        self.preorderTraversal(root.left)
        self.preorderTraversal(root.right)
        return self.res



if __name__ == '__main__':
    td1 = TreeNode(1)
    td2 = TreeNode(2)
    td3 = TreeNode(3)
    td4 = TreeNode(4)
    td5 = TreeNode(5)
    td1.left = td2
    td1.right = td3
    td2.left = td4
    td3.right = td5
    print(Solution().preorderTraversal1(td1))
        