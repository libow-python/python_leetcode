class Solution:
    def getRow(self, rowIndex: int):
        if rowIndex == 0: return [1]
        elif rowIndex == 1: return [1, 1]
        original_list = [1, 1]
        for _ in range(rowIndex-1):
            res = []
            for cur in range(len(original_list) - 1):
                res.append(original_list[cur] + original_list[cur+1])
            res.insert(0, 1)
            res.append(1)
            print(res)
            original_list = res
        return original_list
    
    def getRow1(self, rowIndex: int):
        """
            关键在于从后往前加
            res 代表上次的结果 + 1 从 len - 1 的位置倒着往第一个计算
            row = 2 [1,2,1]
            row = 3 [1, 2, 1, 1] -> 1+2 -> 2+1 -> [1, 3, 3, 1] 从2开始算到1
            主要思想就是先在末行+1, 下一行中间的数, 等于前1行数据的两两之和
        """
        res = []
        for i in range(rowIndex + 1):
            res.append(1)
            if i >= 2:
                print(i, res)
                for j in range(i-1, 0, -1):
                    res[j] += res[j-1]
        return res

if __name__ == '__main__':
    print(Solution().getRow1(4))


            
