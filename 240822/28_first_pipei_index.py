class Solution:
    
    # 漏掉了一种情况
    def strStr(self, haystack: str, needle: str) -> int:
        """
        双指针进行判断
        """
        if not haystack or not needle: return -1
        n_start = 0
        res = []
        for i in range(len(haystack)):
            if needle[n_start] == haystack[i]:
                if n_start == len(needle) - 1:
                    res.append(i-len(needle) + 1)
                    n_start = 0
                else:
                    n_start += 1
            else:
                n_start = 0
        return res[0] if res else -1

    def strStr1(self, haystack: str, needle: str) -> int:
        """
        当遇到mississippi， issip
        strStr 没有考虑字符串重叠的部分直接往后走
        如果失败了需要把h指针回归到最初的位置然后再请进一步
        朴素算法
        对于「朴素匹配」而言，一旦匹配失败，将会将原串指针调整至下一个「发起点」，匹配串的指针调整至起始位置，然后重新尝试匹配。
        """
        if not haystack or not needle: return -1
        h_start = 0
        n_start = 0
        res = []
        while h_start < len(haystack):
            if needle[n_start] == haystack[h_start]:
                if n_start == len(needle) - 1:
                    res.append(h_start-n_start)
                    n_start = 0
                else:
                    n_start += 1
                h_start += 1
            else:
                h_start = h_start - n_start + 1
                n_start = 0
        return res[0] if res else -1
    
    def strStr2(self, haystack: str, needle: str) -> int:
        """find 方法查找字符串中子字符串第一次出现第一个位置的下标"""
        return haystack.find(needle)
    
    def strStr3(self, haystack: str, needle: str) -> int:
        """
        index 方法用于找到字符串中第一次出现的子字符串，并返回子字符串的开始索引
        index 和find 方法的区别
            1. index 找不到会抛出异常
            2. index 可以用在列表，元组，字符串一起使用
        """
        try:
            return haystack.index(needle)
        except:
            return -1
        
    def strStr4(self, haystack: str, needle: str) -> int:
        """
            使用切片正向去判断是否匹配， 只要能匹配到就退出不用再遍历
        """
        for i in range(len(haystack) - len(needle) + 1):
            if haystack[i:i+len(needle)] == needle:
                return i
        else:
            return -1
        
    def strStr5(self, haystack: str, needle: str) -> int:
        """
            使用切片正向去判断是否匹配， 只要能匹配到就退出不用再遍历
            方法4的循环从前截断
        """
        start = 0
        for end in range(len(needle)-1, len(haystack)):
            if haystack[start:end+1] == needle:
                return start
            start += 1
        else:
            return -1
        
    def strStr6(self, haystack: str, needle: str) -> int:
        """
        使用 kmp 算法
        初次认识+理解kmp算法
        1. 在朴素匹配中每次失败匹配需要将起始匹配位置的下一个位置开始, 会来回遍历原始字符串 o(m*n)
        2. kmp 使用next数组去加速这个过程 O(m+n)
            构造next数组
            1. next[0] = 0, p[i] = p[j]; next[i] = j + 1; i+1, j+1
            2. p[i] != p[j]; j = next[j-1] 直到相等或者 j=0
            3. 若j=0, p[i]!=p[j] , next[i] = 0, i+
        """
        # 原始列表构造next数组
        # h_len, n_len = len(haystack), len(needle)
        # i = 0
        # next = [_ for _ in range(h_len)]
        # for j in range(1, h_len):
        #     if haystack[i] != haystack[j]:
        #         while i != 0:
        #             i = next[i-1]
        #             if haystack[i] == haystack[j]:
        #                 break
        #         if i == 0:
        #             next[j] = 0
        #     else:
        #         next[j] = i + 1
        #         i += 1
        # print(next)

        # e = 0
        # for r in range(0, h_len):
        #     while e > 0 and haystack[r] != needle[e]:
        #         print(e, haystack[r], needle[e])
        #         e = next[e]
        #     if haystack[r] == needle[e]:
        #         e += 1
        #     if e == n_len:
        #         return r-n_len
        return -1

            

if __name__ == "__main__":
    # str1, str2 = "mississippi", "issip"
    # str1, str2 = "lusadbutsad", "sad"
    # str1, str2 = "abeabcabfabd", "bfa"
    str1, str2 = "aaabaaabb", "abb"
    print(Solution().strStr6(str1, str2))