import time

map = {}
def climb_stairs_run(n):
    if n == 1:
        return 1
    elif n == 2:
        return 2
    if n in map:
        return map[n]
    result = climb_stairs_run(n-1) + climb_stairs_run(n-2)
    map[n] = result
    return result


if __name__ == '__main__':
    t1 = time.time()
    res = climb_stairs_run(44)
    t2 = time.time()
    print(t2 - t1)
    print(res)
