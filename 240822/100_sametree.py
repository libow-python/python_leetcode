from copy import deepcopy
import collections
class Node:

    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Tree:

    def __init__(self):
        self.preorder_ergodic = []

    def preorder(self, node):
        self.preorder_ergodic.append(node.val)
        if not (node.left or node.right):
            return
        if node.left:
            self.preorder(node.left)
        else:
            self.preorder_ergodic.append(None)
        if node.right:
            self.preorder(node.right)
        else:
            self.preorder_ergodic.append(None)

    def is_same_value(self, node1, node2):
        """
        深度优先遍历
        思想：通过同时以根左右的遍历顺序，深度遍历两棵树，如果遇到不相等的值，则两棵树不同
        """
        if not node1 and not node2: return True;
        if not (node1 and node2): return False;
        if node1.val != node2.val:
            return False
        return self.is_same_value(node1.left, node2.left) and self.is_same_value(node1.right, node2.right)
    
    def is_same_value2(self, node1, node2):
        """
        广度优先遍历
        也可以通过广度优先搜索判断两个二叉树是否相同。同样首先判断两个二叉树是否为空，如果两个二叉树都不为空，则从两个二叉树的根节点开始广度优先搜索。

        使用两个队列分别存储两个二叉树的节点。初始时将两个二叉树的根节点分别加入两个队列。每次从两个队列各取出一个节点，进行如下比较操作。

        比较两个节点的值，如果两个节点的值不相同则两个二叉树一定不同；

        如果两个节点的值相同，则判断两个节点的子节点是否为空，如果只有一个节点的左子节点为空，或者只有一个节点的右子节点为空，则两个二叉树的结构不同，因此两个二叉树一定不同；

        如果两个节点的子节点的结构相同，则将两个节点的非空子节点分别加入两个队列，子节点加入队列时需要注意顺序，如果左右子节点都不为空，则先加入左子节点，后加入右子节点。

        如果搜索结束时两个队列同时为空，则两个二叉树相同。如果只有一个队列为空，则两个二叉树的结构不同，因此两个二叉树不同

        广度优先遍历不适用递归，递归多用于深度, 构建一个队列
        """
        # if not node1 and not node2: return True;
        # if not (node1 and node2): return False;  
        # val, left, right = False, False, False
        # if node1.val == node2.val:
        #     val = True
        #     if node1.left and node2.left:
        #         if node1.left.val == node2.left.val:
        #             left = True
        #     if not node1.left and not node2.left:
        #         left = True
        #     if node1.right and node2.right:
        #         if node1.right.val == node2.right.val:
        #             right = True
        #     if not node1.right and not node2.right:
        #         right = True
        # if val and left and right:
        #     return self.is_same_value2(node1.left, node2.left) and self.is_same_value2(node1.right, node2.right)
        # return False
        if not node1 and not node2: return True;
        if not (node1 and node2): return False;

        p_queue = collections.deque([node1])
        q_queue = collections.deque([node2])

        while p_queue and q_queue:
            print([i.val for i in p_queue], [i.val for i in q_queue], "00000")
            p_f = p_queue.popleft()
            q_f = q_queue.popleft()

            if p_f.val != q_f.val:
                return False
            
            p_f_l = p_f.left
            p_f_r = p_f.right
            q_f_l = q_f.left
            q_f_r = q_f.right

            if p_f_l and q_f_l:
                pass
            elif not p_f_l and not q_f_l:
                pass
            else:
                return False
            
            if p_f_r and q_f_r:
                pass
            elif not p_f_r and not q_f_r:
                pass
            else:
                return False

            if p_f_l:
                p_queue.append(p_f_l)
            if p_f_r:
                p_queue.append(p_f_r)
            if q_f_l:
                q_queue.append(q_f_l)
            if q_f_r:
                q_queue.append(q_f_r)
        return p_queue == q_queue


class Solution:  

    def isSameTree(self, p, q) -> bool:
        if not p and not q: return True
        if not q or not p: return False
        pt = Tree()
        pt.preorder(p)
        pr = pt.preorder_ergodic

        qt = Tree()
        qt.preorder(q)
        qr = qt.preorder_ergodic
        print(pr, qr)
        return pr == qr

if __name__ == '__main__':
    n1 = Node(1)
    n2 = Node(2)
    n3 = Node(3)
    n4 = Node(4)
    n5 = Node(5)
    n2.left = n4
    n3.right = n5
    n1.left = n2
    n1.right = n3

    n6 = Node(1)
    n7 = Node(2)
    n8 = Node(3)
    n9 = Node(4)
    n10 = Node(5)
    n11 = Node(11)
    # n7.left = n11
    # n8.right = n10
    n6.left = n7
    n6.right = n8

    # t = Tree()
    # t.preorder(n1)
    # print(t.preorder_ergodic)
    # r =  Solution().isSameTree(n1, n6)
    r = Tree().is_same_value2(n1, n6)
    print(r)