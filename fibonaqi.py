# 动态规划学习


_map = {}

def fib(n):
    # 备忘录
    if n ==0: return 0
    if n == 1 or n == 2: return 1
    ls = fib(n-1) + fib(n-2)
    print(_map, "map",n) # {3: 2, 4: 3} map
    # 也相当于 从小往大了算，到了深层递归ls可以获取到值
    if not _map.get(n):
        _map[n] = ls
    else:
        return _map.get(n)
    return ls

def ff(n):
    if n ==0: return 0
    if n == 1 or n == 2: return 1
    return ff(n-1) + ff(n-2)


if __name__ == '__main__':
    import time
    t1 = time.time()
    s = fib(10)
    t2 = time.time()
    print(s)
    t3 = time.time()
    b = ff(5)
    print(b)
    t4 = time.time()
    print(round(t2-t1,5),round(t4-t3,5),"time")