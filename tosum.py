def twoSum(nums, target):
    """两数之和 ----- 哈希"""
    # res_dict = {}
    # res_lst = []
    # for  index, num in enumerate(nums):
    #     res = target - num
    #     if res_dict.get(res) is not None:
    #         res_lst.append(res_dict[res])
    #         res_lst.append(index)
    #     else:
    #         res_dict[num] = index
    # return res_lst

    """两数之和，指针法"""

    left = 0
    right = len(nums) - 1
    lst = []
    while left <= right:
        print(left,right)
        if nums[left] + nums[right] - target < 0:
            left += 1
        elif nums[left] + nums[right] - target > 0:
            right -= 1
        else:
            lst.append(left)
            lst.append(right)
            left += 1
    return lst


if __name__ == '__main__':
    nums = [2, 7, 11 ,15]
    target = 14
    print(twoSum(nums, target))