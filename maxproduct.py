def maxProduct(str_a) -> int:
    pass
    # max_lst = [0]
    # 直接循环range(len(str_a)) 效率会更好一点
    # for key,s_a in enumerate(str_a):
    #     for now_a in str_a[key+1:]:
    #         flag = True
    #         for i in s_a:
    #             if i in now_a:
    #                 flag = False
    #         if flag:
    #             total = len(s_a) * len(now_a)
    #             max_lst.append(total)
    # print(max_lst)
    # return max(max_lst)
    #
    # 改进思路
    """
    1.将a-z转换成1的左移，b 相当 1左移一位
    2. 两层循环去按位与运算如果不为 0， 就代表有字母是相同的
    # abc
    # 111 -> 相当于 7 这个数就相当于是abc的代表数 ac 相当101-> 5 
    """
    # now_bit_dict= {}
    # for s_a in str_a:
    #     total = 0
    #     for zm in s_a:
    #         total |= 1 << ord(zm) - ord("a") # 关键或运算-》在一个字符串中有相同的可以排除去掉,确保字符串是有不同的字母组合而成 aaa-> a
    #     now_bit_dict[s_a] = total
    # print(now_bit_dict)
    # max = 0
    # for k,v in enumerate(str_a):
    #     for j in str_a[k+1:]:
    #         if now_bit_dict[v] & now_bit_dict[j] == 0:
    #             max = len(v) * len(j) if len(v) * len(j) > max else max
    # print(max)
    # return max

    '''优化'''
    now_bit_dict= []
    for s_a in str_a:
        total = 0
        for zm in s_a:
            total |= 1 << ord(zm) - ord("a") # 关键或运算-》在一个字符串中有相同的可以排除去掉,确保字符串是有不同的字母组合而成 aaa-> a
        now_bit_dict.append(total)
    print(now_bit_dict)
    max = 0
    # 新的列表和实参一一对应
    for i in range(len(now_bit_dict)):
        for j in range(i + 1,len(now_bit_dict)):
            if now_bit_dict[i] & now_bit_dict[j] == 0:
                max = len(str_a[i]) * len(str_a[j]) if len(str_a[i]) * len(str_a[j]) > max else max
    print(max)
    return max


if __name__ == '__main__':
    # print(maxProduct(["a","ab","abc","d","cd","bcd","abcd"]))
    # print(maxProduct(["a","aa","aaa","aaaa"]))
    print(maxProduct(["abcw","baz","foo","bar","xtfn","abcdef"]))

