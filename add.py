# 累加
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

def add(num1,num2):
	if num2 == 1:
		return num1 + 1
	x = num1 + num2
	return add(x, num2 - 1)

class AddAPIView(ViewSet):
	def to_add_num(self, request):
		n = request.data.get("number")
		if n <= 1:
			return Response({
				"return": n
			})
		x = add(n, n-1)
		return Response({
			"return" : x
		})

