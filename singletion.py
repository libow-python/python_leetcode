# 单例模式
# import threading
# class Singleton(object):
#     _instance_lock = threading.Lock()
#
#     def __init__(self):
#         pass
#
#     def __new__(cls, *args, **kwargs):
#         if not hasattr(Singleton, "_instance"):
#             with Singleton._instance_lock:
#                 if not hasattr(Singleton, "_instance"):
#                     Singleton._instance = object.__new__(cls)
#         return Singleton._instance
#
# obj1 = Singleton()
# obj2 = Singleton()
# print(obj1,obj2)
#
# def task(arg):
#     obj = Singleton()
#     print(obj)
#
# for i in range(10):
#     t = threading.Thread(target=task,args=[i,])
#     t.start()


class Sibglation(object):

    def __new__(cls, *args, **kwargs):
        if not hasattr(Sibglation, "_instance"):
            Sibglation._instance = object.__new__(cls)
        return Sibglation._instance

    def __init__(self):
        pass


a = Sibglation()
b = Sibglation()
print(id(a), id(b))
