# def threeSum(numsList) -> list:
#     """三数之和，升级版二数之和"""
#     if not numsList: return []
#     res_lst = []
#     # 首先遍历原始数组
#     for key, num in enumerate(numsList):
#         array_lst = numsList[key + 1:]
#         # 开始两数之和
#         now_dict = []
#         target = -num
#         for i in array_lst:
#             res = target - i
#             if res not in now_dict:
#                 now_dict.append(i)
#             else:
#                 # 排序看是否一致
#                 rr = sorted([num, i, res])
#                 if rr not in res_lst:
#                     res_lst.append(rr)
#     print(res_lst)
#     return res_lst

def threeSum(numsList) -> list:
    pass
    if not numsList: return []
    res_lst = []
    numsList = sorted(numsList)
    for i in range(len(numsList)):
        left = i + 1
        right = len(numsList) - 1
        while left < right:
            if numsList[i] + numsList[left] + numsList[right] < 0:
                left += 1
            elif numsList[i] + numsList[left] + numsList[right] > 0:
                right -= 1
            else:
                if [numsList[i], numsList[left],numsList[right]] not in res_lst:
                    res_lst.append([numsList[i], numsList[left],numsList[right]])
                    continue
                else:
                    left += 1
    return res_lst


if __name__ == '__main__':
    # print(threeSum([-1, 0, 1, 2, -1, -4]))
    # print(threeSum([0,0,0,0]))
    print(threeSum([-2,0,1,1,2]))
    # -4 -1 -1 0 1 2
