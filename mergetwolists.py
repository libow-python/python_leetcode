# 合并两个有序的链表


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

# 归并
# def mergeTwoLists(list1,list2):
#     if not list1 and not list2: return list1
#     if not list1: return list2
#     if not list2: return list1
#     cur1 = list1
#     cur2 = list2
#     node = ListNode()
#     res_node = node
#     while cur1 != None or cur2 != None:
#         print(cur1.val, cur2.val,"99999")
#         if cur1.val <= cur2.val:
#             node.next = ListNode(cur1.val)
#             cur1 = cur1.next
#         else:
#             node.next = ListNode(cur2.val)
#             cur2 = cur2.next
#         node = node.next
#         if cur1 == None:
#             while cur2:
#                 node.next = ListNode(cur2.val)
#                 node = node.next
#                 cur2 = cur2.next
#                 print(cur2)
#             break
#         if cur2 == None:
#             while cur1:
#                 node.next = ListNode(cur1.val)
#                 node = node.next
#                 cur1 = cur1.next
#             break
#     return res_node.next


# 递归写法
def mergeTwoLists(list1,list2):
    pass
    if not list1: return list2
    if not list2: return  list1
    if list1.val < list2.val:
        list1.next = mergeTwoLists(list1.next, list2) # 相当于做个排序，吧娇小的放在前面，这里一定是大的值
        return list1 # 这里相当于取小的当头结点
    else:
        list2.next = mergeTwoLists(list1, list2.next) # 相当于做个排序，吧娇小的放在前面，这里一定是大的值
        return list2 # 这里相当于取小的当头结点





if __name__ == '__main__':
    list1 = ListNode(-9)
    cur1 = list1
    list1.next = ListNode(3)
    list1 = list1.next
    # list1.next = ListNode(4)
    # list1 = list1.next

    list2 = ListNode(5)
    cur2 = list2
    list2.next = ListNode(7)
    list2 = list2.next
    # list2.next = ListNode(4)

    # print(cur1.val, cur2.val)
    # print(cur1.next.val, cur2.next.val)
    # print(cur1.next.next.val, cur2.next.next.val)

    s = mergeTwoLists(cur1,cur2)
    print(s.val)
    print(s.next.val)
    print(s.next.next.val)
    print(s.next.next.next.val)
    # print(s.next.next.next.next.val)
    # print(s.next.next.next.next.next.val)


