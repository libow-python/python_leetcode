import time
from datetime import datetime


def run():
    today = datetime.today()
    year, month, day = today.year, today.month, today.day
    end_time = datetime(year, month, day, 19)
    while datetime.now() < end_time:
        diff_seconds = int((end_time - datetime.now()).seconds)
        diff_minutes = diff_seconds // 60
        print(diff_minutes)
        time.sleep(60)


if __name__ == '__main__':
    run()
