def longestCommonPrefix(strs) -> str:
    if not strs: return ""
    min_str = strs[0]
    for i in strs:
        if len(i) < len(min_str):
            min_str = i
    st = ""
    for k,v in enumerate(min_str):
        flag = False
        for s in strs:
            if s[k] != v:
                flag = True
                break
        if not flag:
            st += v
        else:
            return st
    return st


if __name__ == '__main__':
    print(longestCommonPrefix(["da","da","da"]))