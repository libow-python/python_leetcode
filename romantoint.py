def romanToInt(s) -> int:
    '''
    罗马数字转整形的基本思想就是
    小数在大数的前面做减法，小数在大数的右面做加法
    '''
    # map = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}
    # total = 0
    # for i in range(len(s)-1):
    #     x = map[s[i]]
    #     y = map[s[i+1]]
    #     if map[s[i]] < map[s[i+1]]:
    #         total -= map[s[i]]
    #     else:
    #         total += map[s[i]]
    # total += map[s[-1]]
    # return total
    # 优化，本质上就是： 小数在大数的前面做减法，小数在大数的右面做加法
    # 优化的思路，把以上思想抽取出来
    map = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000, "a": 4, "b": 9, "c": 40, "d": 90, "e": 400,
           "f": 900}
    s = s.replace("IV","a")
    s = s.replace("IX","b")
    s = s.replace("XL","c")
    s = s.replace("XC","d")
    s = s.replace("CD","e")
    s = s.replace("CM","f")
    total = 0
    print(s)
    # 这种情况是全加，以至于不用去考虑每一位是否加减
    for i in range(len(s)):
        total += map[s[i]]
    return total


if __name__ == '__main__':
    print(romanToInt("MCMXCIV"))
