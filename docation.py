# # 装饰器
#
# import time
# def get_time(func):
# 	def inner():
# 		start = time.time()
# 		print(start)
# 		func()
# 		end = time.time()
# 		print(end)
# 		t = end - start
# 		print(f"一共花费了:{t}")
# 	return inner
#
# @get_time
# def add():
# 	time.sleep(2)
# 	return None
#
#
# def my_decoration(func):
#     print('*'*10,'我是华丽丽的分隔线','*'*10)
#     return func
#
#
# @my_decoration
# def f():
#     print('这是一个函数')
#
# @my_decoration
# def f2():
#     print('这是另一个函数')
#
# def outer(x):
#     def inner():
#         x()     # 这里实际是调用了f1()函数
#     return inner
#
# def f1():
#     print('这是f1函数')
#
# ot = outer(f1)
# ot()
#
# # if __name__ == '__main__':
# # 	# print(add())
# # 	print(f())
# # 	print(f2())

def wrapper():
	lst = []
	for i in range(4):
		def inner(a):
			return i+a
		lst.append(inner)
	return lst

print([x(4) for x in wrapper()])