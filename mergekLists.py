# 多个有序链表拼接
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
# def mergeKLists(lst):
#     pass
#     if not lst: return ListNode()
#     while lst:
#         if 0 < len(lst) < 2: return lst[0]
#         elif len(lst) > 2:
#             first_node = lst[0]
#             second_node = lst[1]
#             last_lst = lst[2:]
#             res_lst = mergeTwoLists(first_node, second_node)
#             # now_lst = []
#             # now_lst.append(res_lst)
#             # for i in last_lst:
#             #     now_lst.append(i)
#             last_lst.insert(0,res_lst)  # 时间复杂度和for 循环一样，达不到想要的效果
#             lst = last_lst
#         else:
#             first_node = lst[0]
#             second_node = lst[1]
#             return mergeTwoLists(first_node, second_node)


def mergeKLists(lst):
    pass
    """归并直到都是1"""
    if not lst: return ListNode()
    return mergeSort(lst,0,len(lst)//2)


# def mergeSort(lst,start,len):
#     if len - 1 == 0:
#         return lst[0]
#     m = len // 2
#     l = mergeSort(lst[:m],0, m)
#     r = mergeSort(lst[m:],0,len-m)
#     return mergeTwoLists(l,r)

def mergeSort(lst,left,right):
    if len(lst) == 1:
        return lst[0]
    m = len(lst) // 2
    l = mergeSort(lst[left:right],0,m)
    r = mergeSort(lst[right:],0,(len(lst)-m)//2)
    return mergeTwoLists(l,r)


def mergeTwoLists(lst1, lst2):
    if not lst1: return lst2
    if not lst2: return lst1
    if lst1.val < lst2.val:
        lst1.next = mergeTwoLists(lst1.next, lst2)
        return lst1
    else:
        lst2.next = mergeTwoLists(lst1, lst2.next)
        return lst2



if __name__ == '__main__':
    list1 = ListNode(1)
    cur1 = list1
    list1.next = ListNode(4)
    list1 = list1.next
    list1.next = ListNode(6)
    list1 = list1.next

    list2 = ListNode(1)
    cur2 = list2
    list2.next = ListNode(3)
    list2 = list2.next
    list2.next = ListNode(4)
    list2 = list2.next

    list3 = ListNode(2)
    cur3 = list3
    list3.next = ListNode(6)
    list3 = list3.next

    lst = [cur1, cur2, cur3]
    print(lst)
    s = mergeKLists(lst)
    print(s)
    print(s.val)
    print(s.next.val)
    print(s.next.next.val)
    print(s.next.next.next.val)
    print(s.next.next.next.next.val)
    print(s.next.next.next.next.next.val)
    print(s.next.next.next.next.next.next.val)
    print(s.next.next.next.next.next.next.next.val)
    # print(s.next.next.next.next.next.next.next.next.val)

    # print(len([cur1]))

