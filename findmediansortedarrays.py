# def findMedianSortedArrays(nums1, nums2) -> int:
#     left1, left2 = 0, 0
#     res_lst = []
#     while nums1 or nums2:
#         if not nums1:res_lst=nums2;break
#         if not nums2:res_lst=nums1;break
#         if nums1[left1] <= nums2[left2]:
#             res_lst.append(nums1[left1])
#             left1 += 1
#         elif nums1[left1] > nums2[left2]:
#             res_lst.append(nums2[left2])
#             left2 += 1
#         if left1 >= len(nums1):
#             res_lst = res_lst + nums2[left2:]
#             break
#         if left2 >= len(nums2):
#             res_lst = res_lst + nums1[left1:]
#             break
#     n = len(res_lst)
#     if n % 2:
#         res = res_lst[n//2]
#     else:
#         res = (res_lst[n//2] + res_lst[n//2-1])/2
#     print(res_lst)
#     return res

# def findMedianSortedArrays(nums1, nums2) -> int:
#     if (len(nums1)>len(nums2)):
#         nums1, nums2 = nums2, nums1
#     m = len(nums1)
#     n = len(nums2)
#
#     totalleft = (m + n + 1) // 2
#
#     left = 0
#     right = m
#
#     while left < right:
#         i = (left + right + 1) // 2
#         j = totalleft - i
#         print(i, j, nums1[i-1], nums2[j])
#         if nums1[i-1] > nums2[j]:
#             right -= 1
#         else:
#             left = i
#     print(left, right)
#     i = left
#     j = totalleft - i
#
#     # print(i,j,"2313")
#
#     # print(i,j,totalleft)
#     max1 = 2**40
#     leftnum1max = 0 if i == 0 else int(nums1[i-1])
#     rightnum1min = max1  if i == m else int(nums2[j])
#     leftnum2max =0 if j==0 else int(nums2[j-1])
#     rightnum2min = max1  if j==m else int(nums1[i])
#     print(leftnum1max,rightnum1min,leftnum2max,rightnum2min)
#     if (m+n)%2:
#         return max(leftnum1max, leftnum2max)
#     else:
#         return (max(leftnum1max, leftnum2max) + min(rightnum1min,rightnum2min)) / 2

def findMedianSortedArrays(nums1, nums2) -> int:
    n = len(nums1)
    m = len(nums2)
    if n > m:
        nums1, nums2 = nums2, nums1
    totalleft = (n + m + 1) // 2
    n = len(nums1)
    m = len(nums2)

    left = 0
    right = n
    while left < right: # 为啥二分查找到左右相等的时候停止
        i = (left + right + 1) // 2
        j = totalleft - i
        if nums1[i-1] > nums2[j]:
            right -= 1
        else:
            left = i

    i = left
    j = totalleft - i

    max1 = 2 ** 40
    leftNums1Max = -max1 if i ==0 else nums1[i - 1]
    leftNums2Max = -max1 if j == 0 else nums2[j - 1]
    rightNums1Min = max1 if i == n else nums1[i]
    rightNums2Min = max1 if j == n else nums2[j]
    print(leftNums1Max,leftNums2Max, "max")
    print(max(leftNums1Max,leftNums2Max))
    print(rightNums1Min,rightNums2Min, "min")
    if (n + m) % 2:
        return max(leftNums1Max, leftNums2Max)
    else:
        res = (max(leftNums1Max, leftNums2Max) + min(rightNums1Min,rightNums2Min)) / 2
        return res

if __name__ == '__main__':
    # print(findMedianSortedArrays([1, 3, 6, 7, 9,11], [2, 4, 4, 5, 9, 10]))
    print(findMedianSortedArrays([1,3], [2]))
