def convert_z(s, numRows) -> str:
    if len(s) == 1: return s
    group_size  = numRows * 2 - 2
    res_dict = {}
    for i in range(len(s)):
        mod = i % group_size
        if mod < numRows:
            mod = mod
        else:
            mod = group_size - mod
        if res_dict.get(mod):
            res_dict[mod].append(s[i])
        else:
            res_dict[mod] = [s[i]]
    res_str = ""
    for k,v in res_dict.items():
        res_str += "".join(v)
    return res_str

if __name__ == '__main__':
    print(convert_z('PAYPALISHIRING', 3))