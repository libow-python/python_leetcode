class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


def addTwoNumbers(l1, l2):
    start = curr = ListNode()
    carry = 0
    while l1 or l2:
        value_l1 = l1.val if l1 else 0
        value_l2 = l2.val if l2 else 0
        total = value_l1 + value_l2 + carry
        curr.next = ListNode(total % 10)
        curr = curr.next
        carry = total // 10
        if l1:
            l1 = l1.next
        if l2:
            l2 = l2.next
        if carry:
            curr.next = ListNode(carry)
    return start.next


if __name__ == '__main__':
    dummy = curr = ListNode()  # dummy和curr公用了listcode的地址
    print(dummy, curr)
    curr.next = ListNode(2)  # 是在dummy = curr = ListNode()上修改，
    curr = curr.next  # 令起空间
    print(dummy, curr)
    print(dummy.val, curr.val)
    curr.next = ListNode(3)
    curr = curr.next
    print(dummy.val, curr.val)
    print(dummy.next.next.val)
