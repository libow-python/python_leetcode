def rotate_list(nums, k):
    """简单的在原list上处理"""
    for i in range(k):
        value = nums.pop(-1)
        nums.insert(0, value)
    print(nums)
    return nums


def rotate_list2(nums, k):
    """
    切片
    当我们将数组的元素向右移动 k 次后，尾部 k mod n 个元素会移动至数组头部，其余元素向后移动 k mod n 个位置
    特点 1: 考虑 k > len(nums)
        2: 考虑 k < len(nums)
        3: 考虑 k = len(nums)
        结论：取模运算
    """
    n = len(nums)
    k %= n
    reversed_nums, after_nums = nums[-k:], nums[:-k]
    nums[:] = reversed_nums + after_nums
    return nums


def rotate_list3(nums, k):
    """
    环状替代: 加深数学理解程度多读

    """
    pass


if __name__ == '__main__':
    a = [1, 2]
    ks = 3
    print(rotate_list2(a, ks))
