def jump_game(nums):
    """
    思路：动态规划，从后面出发，只要能找到前一个能到达目标的位置，那么就可以把问题转换成 是否能到达前一个位置，一直缩小，
         只要在某一个点到达不了，他整个通路就是断的，代码执行完成那么就是可行
    注意： 代码思路不够清晰 ，先想清楚整个流程
    coding 能力还需加强
    """
    end = len(nums) - 1  # end 指的是最后的位置也就是题目中能否到达位置
    while end >= 1:
        is_reach = False
        for i in range(1, end + 1):  # i 指的是步长
            if nums[end - i] >= i:
                is_reach = True
                end -= i
                break
            else:
                is_reach = False

        if not is_reach:
            return False
    return True


def jump_game2(nums):
    """
    ！！！！ 很牛逼的思路 ！！！！
    也是动态规划思想
    如果某一个作为起跳点的格子可以跳跃3， 那么表示后面的三个格子都可以做为起跳点
    对每一个能作为起跳点的格子都跳一遍，能跳的最远距离更新

    k 可以表示路有多长
    i 可以表示成棋子走的位置
    i + nums[i] 表示棋子从现在能走多远 (和路进行比较，只要有路就能到达， 所以只要比较 i 和 k 就行)
    max(k, i+nums[i]) 表示当前最长的路

    动态规划算法：
        定义子问题：dp[i]表示前i个节点的能跳跃到的最大距离;  也就是下面的k
    更新公式：
        dp[i] = max{ dp[i - 1] , i + nums[i] }
        max{ 前i - 1个节点的最大距离, 当前节点能跳到的距离 }
    初始化：
        dp[0] = nums[0];
        从下表1的节点开始执行：
    执行过程：
        1.进入第i个节点前:dp[i - 1] >= i ?
            判断当前节点是否可达：根据能否从前i - 1个节点跳跃过来
        2.根据公式更新dp[i]
        返回：
            dp[len - 1] >= len - 1;

    然后很显然，dp[i]只和dp[i-1]有关, 所以可以优化空间，只需要一个变量rightMax就行
    """
    k = 0
    for i in range(len(nums)):
        if i > k:
            return False
        k = max(k, i + nums[i])
    return True


if __name__ == '__main__':
    n = [2, 3, 1, 1, 4]  # True
    # n = [3, 2, 1, 0, 4]  # False
    # n = [1, 1, 0, 2, 0, 4]  # False
    print(jump_game(n))
