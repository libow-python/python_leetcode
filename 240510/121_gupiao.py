import time


def best_stock_trading_price(prices):
    """
    暴力破解
    """
    max_price = 0
    for i in range(len(prices) - 1):
        for j in range(i+1, len(prices)):
            earning = prices[j] - prices[i]
            if earning < 0:
                continue
            if earning > 0 and max_price < earning:
                max_price = earning
    print(max_price)


def best_stock_trading_price2(prices):
    """
    动态规划
    使用动态规划计算出某个范围的最大值
    某一天以及后面的的范围内的最大值
    """
    max_price = 0
    max_p = 0
    for i in range(len(prices) - 1, -1, -1):
        max_p = max(max_p, prices[i])
        if prices[i] < max_p:
            max_price = max(max_p - prices[i], max_price)
    print(max_price)
    return max_price


def best_stock_trading_price3(prices):
    """
    记录最大最小值

    把最小值从第一个值开始, 边遍历边找最大值， 一趟就可以找到所有， 也是动态规划
    """
    # min_price = float('inf')   # 正无穷大
    min_price = prices[0]
    max_price = 0

    for price in prices:
        if price < min_price:
            min_price = price
        if price - min_price > max_price:
            max_price = price - min_price
    return max_price


if __name__ == '__main__':
    prices_list = [7, 1, 4, 3, 1]
    print(best_stock_trading_price3(prices_list))
