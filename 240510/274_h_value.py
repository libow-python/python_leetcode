def calculate_h_value(citations):
    h = {}
    for citation in citations:
        if not citation:
            continue
        if citation not in h:
            h[citation] = 1
        for k, v in h.items():
            if k <= citation:
                h[k] += 1
    print(h)


if __name__ == '__main__':
    n = [3, 0, 6, 1, 5]  # 0， 1， 3， 5 ，6
    calculate_h_value(n)
