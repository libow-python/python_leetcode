from typing import List


def jump_games(nums: List[int]):
    """
        ？： 如何判断执行可以到达
            如何停止

        思路一：
            倒着走，找跨度最大的节点， 重点是从后面走知道什么是跨度最大的， 动态规划
            自己能想到的第一个方法
    """
    end = len(nums) - 1
    count = 0
    while end > 0:
        reach_step = 0
        new_end = end
        for i in range(end - 1, -1, -1):
            if nums[i] >= end - i:
                reach_step = max(reach_step, end - i)
                new_end = i
        if not reach_step:
            return 0
        end = new_end
        count += 1
    return count


def jump_games2(nums: List[int]):
    """
    思考： 该如何进行优化
    优化第一种，如何找到距离目标位置最大跨度

    结果：更慢了
    """
    end = len(nums) - 1
    count = 0
    while end > 0:
        longest = float('inf')
        is_reachable = False
        for i, v in enumerate(nums[:end]):
            if i + nums[i] >= end:
                longest = min(longest, i)
                is_reachable = True
        if not is_reachable:
            return 0
        end = longest
        count += 1
    return count


def jump_games3(nums: List[int]):
    """
    正向查找可到达的最大位置， 前提一定是可以走到最后，是通路
    正向贪心算法：
    此题解也就是题目中，所描述的是此跳跃一定可以跳到最后一个， 没有短路的情况
    [2, 3, 1, 1, 4] 为例子
    1. 从2 开始起跳， 那么也就以为这第一次起跳我会在ind:1或者是在ind:2上面, 知道走到ind:2上面后认为我这次跳跃结束
    2. 遍历往后走3， 可以跳跃到1+3 = 4的位置上，所以对于3 来说， ind:2 -> ind:4 是起跳点， 以此往后推 每一次的跳跃范围是 [next:ind+nums[ind]]
    3. 直到遍历完为止

    从0的位置跳跃，可知道第一次跳跃 可以到 3，1
    记录最大能跳跃的值(3 和1 谁跳跃的最大就从谁开始跳)，题目中为3, 也就是说从3 开始再跳跃，每次走到最后一个值，+ 1， 知道走完位置, 因为一定可以到达，所以要不正好可以在位置上，要不就是路过
    只有找到第一个位置的最长距离后，才会跳下一步(一开始也要跳一步， 把起点当作上一次的终点)
    """
    jumps = 0
    current_end = 0
    farthest = 0  # 代表着最长的位置，每此遇到最长位置就表示可以最大跳到某个位置， 记录每一个跳跃范围内的最大值

    for i in range(len(nums) - 1):
        farthest = max(farthest, i + nums[i])

        if i == current_end:
            jumps += 1
            current_end = farthest
    return jumps


if __name__ == '__main__':
    # n = [2, 3, 1, 1, 4]  # 2
    # n = [1]  # 0
    # n = [1, 2, 1, 1, 1]  # 3
    # n = [2, 1, 0, 3, 1]  # 3
    # n = [3, 2, 1, 0, 1]
    # n = [2, 2, 3, 1, 2, 3, 1]
    n = [2, 5, 1, 1, 2, 1, 1]
    print(jump_games3(n))
