"""
给你一个整数数组 prices ，其中 prices[i] 表示某支股票第 i 天的价格。

在每一天，你可以决定是否购买和/或出售股票。你在任何时候 最多 只能持有 一股 股票。你也可以先购买，然后在 同一天 出售。

返回 你能获得的 最大 利润 。
"""


def max_prices(prices):
    """尝试找各个递增区间，递增区间的最大-最小就是局部最由解决？"""
    slow, fast = 0, 1
    if len(prices) <= 1:
        return 0
    max_p = 0
    f = True  # 是否是递增
    while fast < len(prices):
        if prices[fast - 1] < prices[fast]:
            fast += 1
        elif prices[fast - 1] >= prices[fast]:
            f = False
            max_p += prices[fast - 1] - prices[slow]
            slow = fast
            fast += 1

    print(max_p)
    return max_p


def max_prices2(prices):
    """
    尝试找各个递增区间，递增区间的最大-最小就是局部最由解决？
    方法改进： 剖析上面的递增区间，是不是可以看做只要后一个比前一个数大我就把差值相加
    """
    max_p = 0
    for i in range(1, len(prices)):
        if prices[i] > prices[i - 1]:
            max_p += prices[i] - prices[i - 1]
    print(max_p)
    return max_p


if __name__ == '__main__':
    # p = [7, 1, 5, 3, 6, 4]  # 7
    p = [1, 2, 3, 4, 5]  # 4
    # p = [6, 1, 3, 2, 4, 7]  # 7
    max_prices2(p)
