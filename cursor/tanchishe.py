# Initialize Pygame
import pygame
import random
pygame.init()

# Set up the game window
window_width = 800
window_height = 600
game_window = pygame.display.set_mode((window_width, window_height))
pygame.display.set_caption("Snake Game")

# Set up the colors
white = (255, 255, 255)
black = (0, 0, 0)
red = (255, 0, 0)

# Set up the font
font = pygame.font.SysFont(None, 50)

# Set up the clock
clock = pygame.time.Clock()

# Set up the snake
snake_block_size = 10
snake_speed = 15
snake_list = []
snake_length = 1
snake_x = window_width / 2
snake_y = window_height / 2
snake_x_change = 0
snake_y_change = 0

# Set up the food
food_block_size = 10
food_x = round(random.randrange(0, window_width - food_block_size) / 10.0) * 10.0
food_y = round(random.randrange(0, window_height - food_block_size) / 10.0) * 10.0

# Set up the game loop
game_over = False
while not game_over:
    # Handle events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            game_over = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                snake_x_change = -snake_block_size
                snake_y_change = 0
            elif event.key == pygame.K_RIGHT:
                snake_x_change = snake_block_size
                snake_y_change = 0
            elif event.key == pygame.K_UP:
                snake_y_change = -snake_block_size
                snake_x_change = 0
            elif event.key == pygame.K_DOWN:
                snake_y_change = snake_block_size
                snake_x_change = 0

    # Move the snake
    snake_x += snake_x_change
    snake_y += snake_y_change
    if snake_x >= window_width or snake_x < 0 or snake_y >= window_height or snake_y < 0:
        game_over = True
    snake_head = []
    snake_head.append(snake_x)
    snake_head.append(snake_y)
    snake_list.append(snake_head)
    if len(snake_list) > snake_length:
        del snake_list[0]

    # Check for collision with food
    if snake_x == food_x and snake_y == food_y:
        food_x = round(random.randrange(0, window_width - food_block_size) / 10.0) * 10.0
        food_y = round(random.randrange(0, window_height - food_block_size) / 10.0) * 10.0
        snake_length += 1

    # Draw the game window
    game_window.fill(white)
    pygame.draw.rect(game_window, red, [food_x, food_y, food_block_size, food_block_size])
    for block in snake_list:
        pygame.draw.rect(game_window, black, [block[0], block[1], snake_block_size, snake_block_size])

    # Update the display
    score = font.render("Score: " + str(snake_length - 1), True, black)
    game_window.blit(score, [0, 0])
    pygame.display.update()

    # Set the game speed
    clock.tick(snake_speed)

# Quit Pygame
pygame.quit()