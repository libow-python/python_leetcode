# 最接近三数之和
def threeSumClosest(nums, target):
    # 最主要的思想还是，1，排序（降低复杂度）2. 选择基准元素，然后剩下两个元素做指针，二分查
    if not nums: return []
    nums = sorted(nums)
    min_abs = 2 ** 40
    res = 0
    for i in range(len(nums) - 2):
        cur_num = nums[i]
        left = i + 1
        right = len(nums) - 1
        while left < right:
            _cur = nums[left] + nums[right] + cur_num
            cur_abs = abs(target-_cur)
            if cur_abs < min_abs:
                min_abs = cur_abs
                res = _cur
            if _cur < target:
                left += 1
            elif _cur > target:
                right -= 1
            else:
                res = _cur
                break
    return res


if __name__ == '__main__':
    print(threeSumClosest([0,1,2 ], 3))
