class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def reverseList(self, head: ListNode) -> ListNode:
            if not head: return head
            cur = head
            now_node = ListNode(cur.val)
            cur = cur.next
            while cur != None:
                node = ListNode(cur.val)
                node.next = now_node
                now_node = node
                cur = cur.next
            return now_node


if __name__ == '__main__':
    lst = [1,2,1]
    node = ListNode(1)
    cur = node
    for i in lst:
        node.next = ListNode(i)
        node = node.next

    s = Solution()
    v = s.reverseList(cur)
    print(v.val,v.next.val,v.next.next.val,v.next.next.next.val)