def lengthOfLongestSubstring(s) -> int:
    pass
    # if len(s) == 1:
    #     return 1
    # res_lst = []
    # for i in range(len(s)):
    #     now_str = s[i]
    #     for j in range(i+1,len(s)):
    #         if s[j] not in now_str:
    #             now_str = now_str + s[j]
    #         else:
    #             break
    #     res_lst.append(now_str)
    # print(res_lst)
    # max = 0
    # if res_lst:
    #     for i in res_lst:
    #         if len(i)> max:
    #             max = len(i)
    # return max
    """
    大体思想：找一个空的字典，记录下每一个字母的位置，如果下一次出现改字母就截取中间这一段，可以确保一定是无重复，滑动窗口
    """
    st = {} # st 记录着上一次相同字母的位置
    i, ans = 0, 0 # i 其实是作为一个记录上一次重复的位置
    for j in range(len(s)):
        if s[j] in st:
            i = max(st[s[j]], i)
        ans = max(ans, j - i + 1)
        st[s[j]] = j + 1 # 这里的加一是为了确保字符串只有一个的时候， 返回0
    return ans

if __name__ == '__main__':
    print(lengthOfLongestSubstring("abbcdabcbb"))
    # print(lengthOfLongestSubstring(" "))