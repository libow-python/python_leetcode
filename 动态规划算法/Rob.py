# 打家劫舍问题 1
# [2,7,9,3,1]
memo = {}
def rob(lst, index) -> int:
    pass
    # dp
    # n = len(lst)
    # max_len = lst[:]
    # for i in reversed(range(n)):
    #     now_max = max_len[i]
    #     for j in range(i+2,n):
    #         now_max =max(now_max, max_len[i] + max_len[j])
    #     max_len[i] = max(max_len[i],now_max)
    # print(max_len)
    # return max(max_len)

    # 记忆 + 递归
    # 跳出循环的条件
    n = len(lst)
    if index + 2 >= n:
        return lst[index]
    max_num = lst[index]
    for i in range(index + 2, n):
        if memo.get(i):
            max_num =  max(max_num, lst[index] + memo.get(i))
        else:
            num = rob(lst, i)
            memo[i] = num
            max_num = max(max_num, lst[index] + num)
    return max_num

def first(lst):
    n = len(lst)
    L = []
    for i in range(n):
        res = rob(lst, i)
        L.append(res)
    print(L)
    return max(L)


if __name__ == '__main__':
    lst = [2, 7, 9, 3, 1]
    # lst = [2, 1, 1, 2]
    print(first(lst))
    print(memo)
