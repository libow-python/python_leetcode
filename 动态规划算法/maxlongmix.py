# 连续子序列的最大长度和
# [3, -4, 2, -1, 2, 6, -5, 4]  2, -1, 2, 6 --> 9

# 运用的迭代
# def maxLongMix(lst):
#     pass
#     n = len(lst)
#     L = lst[:]
#
#     for i in reversed(range(n)):
#         if i + 1 < n:
#             L[i] = max(L[i], L[i] + L[i+1])
#     print(L)
#     return max(L)

# 运用递归写法
def maxLongMix(lst,i):
    # i 代表当前的索引
    pass
    # 递归的思想就是，当前数，加上下一个数的最大子序列得到的就是当前位置最大的
    n = len(lst)
    if i + 1 >= n:
        return lst[i]
    else:
        now_num = lst[i] + maxLongMix(lst, i + 1)
        max_num = max(lst[i], now_num)
    return max_num

    # error
    # for i in range(n):
    #     #     now_num = lst[i] + maxLongMix(lst, i + 1)
    #     #     max_num = max(lst[i], now_num)   在这里循环的话max_num的值会发生变化并不是当前的

def first(lst):
    n = len(lst)
    lv = []
    for i in range(n):
        s = maxLongMix(lst, i)
        lv.append(s)
    print(lv)
    return max(lv)

if __name__ == '__main__':
    lst = [3, -4, 2, -1, 2, 6, -5, 4]
    # lst = [3, -4, 2]
    #      8  5  9   7  8   6  -1  4
    # print(maxLongMix(lst,0))
    print(first(lst))
