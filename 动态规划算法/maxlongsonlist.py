# 最长递增子序列
# [1,5,2,4,3]
memo = {}


# def maxLong(lst, i):
#     """此函数找的就是当前下标的数，可以组成最长的递增子串的长度"""
#     """记录了索引位置为i,i的最长递增子序列"""
#     if i in memo:
#         return memo[i]
#     if i == len(lst) - 1: return 1
#     max_res = 1 # 相当于自身 1
#     for j in range(i + 1, len(lst)):
#         if lst[j] > lst[i]:
#             max_res = max(max_res, maxLong(lst, j) + 1) #这里加一相当于算上了i自身
#     memo[i] = max_res
#     return max_res

# 迭代写法，从后往前计算
def maxLong(lst):
    pass
    n = len(lst)
    L = [1 for _ in range(n)]

    for i in reversed(range(n)):
        for j in range(i + 1, n):
            if lst[i] < lst[j]:
                L[i] = max(L[i], L[j]+1)
    print(L)
    return max(L)

if __name__ == '__main__':
    lst = [0, 1, 0, 3, 2, 3]
    # 0, 1, 0, 3, 2, 3
    # print(max([maxLong(lst, i) for i in range(len(lst))]))
    print(maxLong(lst))
