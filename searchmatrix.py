def searchMatrix(s, target) -> bool:
    '''搜索二维矩阵
    每行中的整数从左到右按升序排列。
    每行的第一个整数大于前一行的最后一个整数。
    '''

    # for i in s:
    #     if target in i:
    #         return True
    # else:
    #     return False
    if not s: return False
    cols = len(s)
    rows = len(s[0])
    left = 0
    hight = cols * rows - 1
    while left <= hight:
        mid = (left + hight) // 2
        mid_c = mid // rows
        mid_r = mid % rows
        if s[mid_c][mid_r] > target:
            hight -= 1
        elif s[mid_c][mid_r] < target:
            left += 1
        else:
            return True
    return False



if __name__ == '__main__':
    # lst = [
    #     [1, 3, 5, 7],
    #     [10, 11, 16, 20],
    #     [23, 30, 34, 50]
    # ]
    lst = [[1]]
    print(searchMatrix(lst, 0))