def addBinary(one_str, two_str) -> str:
    first = len(one_str)
    second = len(two_str)
    carry = 0
    res_str = ''
    while first > 0 or second > 0:
        first_tail = int(one_str[first-1]) if first else 0
        second_tail = int(two_str[second-1]) if second else 0

        total = first_tail + second_tail + carry

        tail = total % 2

        res_str = str(tail) + res_str

        carry = total // 2

        if first:first -= 1
        if second:second -= 1
    if carry:
        res_str = str(carry) + res_str
    return  res_str



if __name__ == '__main__':
    print(addBinary('11', '1'))