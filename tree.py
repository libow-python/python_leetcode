class Tree(object):
	def __init__(self, item, left=None, right=None):
		self.item = item
		self.left = left
		self.right = right


# 前序遍历二叉树
def first_pr(tree):
	pass
	# 根左右
	stack = list()
	stack.append(tree)
	res_lst = []
	while stack:
		now = stack.pop()
		if now.right:
			stack.append(now.right)
		if now.left:
			stack.append(now.left)
		res_lst.append(now.item)
	return res_lst


def zhong_pr(tree):
	# 左跟右
	stack = []
	while stack or tree:
		while tree:
			stack.append(tree)
			tree = tree.left
		tree = stack.pop()
		print(tree.item)
		tree = tree.right
	return stack


def zhong_2(tree):
	stack = []
	res_lst = []
	while stack or tree:
		while tree:
			stack.append(tree)
			tree = tree.left
		tree = stack.pop()
		res_lst.append(tree.item)
		tree = tree.right
	return res_lst

def end_pri(tree):
	# 左右根
	pass
	stack = []
	res_lst = []
	while stack or tree:
		while tree:
			stack.append(tree)
			if tree.left:
				tree = tree.left
			else:
				tree = tree.right
		s = stack.pop()
		if s:
			res_lst.append(s.item)
		if stack and s==stack[-1].left:
			tree = stack[-1].right
		else:
			tree = None
	return res_lst

#递归写法
def dg_first(tree):
	if not tree:
		return
	print(tree.item)
	dg_first(tree.left)
	dg_first(tree.right)

def zhong_dg(tree):
	if not tree:
		return
	zhong_dg(tree.left)
	print(tree.item)
	s = tree.item
	zhong_dg(tree.right)


def end_dg(tree):
	if not tree:
		return
	end_dg(tree.left)
	end_dg(tree.right)
	print(tree.item)

if __name__ == '__main__':
	t = Tree("A")
	t1 = Tree("B")
	t2 = Tree("C")
	t3 = Tree("D")
	t4 = Tree("E")
	t5 = Tree("F")

	t.left = t1
	t.right = t2

	t1.left = t3
	t1.right = t4

	t2.right = t5
	# print(first_pr(t))
	# print(zhong_pr(t))
	# print(zhong_2(t))
	# print(end_pri(t))
	# print("先序")
	# print(dg_first(t))
	# print("中序")
	print(zhong_dg(t))
	# print("后序")
	# print(end_dg(t))