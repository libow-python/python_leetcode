# 关于链表的算法题
class Node():
    def __init__(self, ele):
        self.ele = ele
        self.next = None


# 关于链表
class SingleList():
    def __init__(self, node=None):
        self._head = node

    def is_empty(self):
        return self._head == None

    def length(self):
        if self.is_empty():
            return 0
        count = 0
        cur = self._head
        while cur != None:
            count += 1
            cur = cur.next
        return count

    def travel(self):
        lst = []
        cur = self._head
        while cur != None:
            # print(cur.ele)
            lst.append(cur.ele)
            cur = cur.next
        print(lst)
        return lst

    def append(self, item):
        """尾插法"""
        now_node = Node(item)
        if self.is_empty():
            self._head = now_node
        else:
            cur = self._head
            while cur.next != None:
                cur = cur.next
            cur.next = now_node

    def add(self, item):
        """头插法"""
        now_node = Node(item)
        if self.is_empty():
            self._head = now_node
        else:
            now_node.next = self._head
            self._head = now_node

    def _head(self):
        return self._head

    def insert(self, index, item):
        if index <= 0:
            self.add(item)
        elif index >self.length():
            self.append(item)
        else:
            now_node = Node(item)
            count = 0
            cur = self._head
            while cur:
                if count == index -1:
                    cur_next = cur.next
                    cur.next = now_node
                    now_node.next = cur_next
                    break
                count += 1
                cur = cur.next

    def remove(self, item):
        cur = self._head
        if cur.ele == item:
            self._head = cur.next
        while cur != None:
            if cur.next:
                if cur.next.ele == item:
                    cur.next = cur.next.next
                    break
            else:
                break
            cur = cur.next

    def set_head(self, node):
        self._head = node

    def search(self, item):
        cur = self._head
        while cur != None:
            if cur.ele == item:
                return True
            cur = cur.next
        return False

    # 删除链表的倒数第 N 个结点
    def removeNthFromEnd(self, singlelst, n):
        """前后指针法"""
        cur = singlelst._head
        left, right = cur, cur
        count = 0
        tag = False
        while right != None:
            if count < n + 1:
                right = right.next
            else:
                left = left.next
                right = right.next
                tag = True
            count += 1

        # 现在的left就是要删除的位置的前一个
        if tag:
            left.next = left.next.next
        else:
            #z n+1 这是一个临界值如果等于n+1这个位置正好就是删除位
            """在遍历while结束的时候，结束位也是n+1"""
            if count != n + 1:
                singlelst.set_head(left.next)
            else:
                left.next = left.next.next
        return singlelst

    def sortLst(self, singlelst):
        cur = singlelst._head
        now_node = Node(cur)
        now_lst = SingleList()
        while cur != None:
            if cur.ele == now_node.ele:
                now_lst.set_head(cur.ele)
            else:
                cur_head = now_lst._head
                now_lst.set_head(Node(cur.ele))
                head = now_lst._head
                head.next = cur_head
            cur = cur.next
        return now_lst


if __name__ == '__main__':
    node = Node(1)
    l = SingleList(node)
    # l.append(2)
    # l.append(3)
    # l.append(4)
    # l.append(5)
    # l.append(6)
    # l.add(0)
    l.append(1)
    l.append(2)
    l.append(1)
    l.travel()
    s = l.sortLst(l)
    s.travel()
    l.removeNthFromEnd(l,2)
    l.travel()


