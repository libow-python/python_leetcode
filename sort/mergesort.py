# 归并排序

def mergesort(lst):
    if not lst: return []
    n = len(lst)
    if n <= 1:
        return lst
    m = n // 2
    l = mergesort(lst[:m])
    r = mergesort(lst[m:])

    l_p, r_p = 0, 0
    res = []
    while l_p < len(l) and  r_p < len(r):
        if l[l_p] < r[r_p]:
            res.append(l[l_p])
            l_p += 1
        else:
            res.append(r[r_p])
            r_p +=1

    res = res + r[r_p:]
    res = res + l[l_p:]
    return res






if __name__ == '__main__':
    lst = [3, 1, 4, 6, 8, 3, 4]
    print(mergesort(lst))
