# def letterCombinations(nums):
#     """电话数的字母组合"""
#     pass
#     number_map = {
#         "1": "",
#         "2": ["a","b","c"],
#         "3": ["d","e","f"],
#         "4": ["g","h","i"],
#         "5": ["j","k","l"],
#         "6": ["m","n","o"],
#         "7": ["p","q","r","s"],
#         "8": ["t","u","v"],
#         "9": ["w","x","y","z"],
#     }
#     n = len(nums)
#     if n == 0: return []
#     if n == 1: return number_map[nums]
#     lst = [number_map[x] for x in nums]
#     while len(lst) > 1:
#         left_lst = lst[:2]
#         right_lst = lst[2:]
#         now_lst = two_add(left_lst[0],left_lst[1])
#         lst = []
#         lst.append(now_lst)
#         if right_lst:
#             for i in right_lst:
#                 lst.append(i)
#     return lst[0]
#
# def two_add(lst1, lst2):
#     now_lst = []
#     for i in lst1:
#         for j in lst2:
#             now_lst.append(i+j)
#     return now_lst

    # 还可以使用回溯
def letterCombinations(nums):
    number_map = {
        "1": "",
        "2": ["a","b","c"],
        "3": ["d","e","f"],
        "4": ["g","h","i"],
        "5": ["j","k","l"],
        "6": ["m","n","o"],
        "7": ["p","q","r","s"],
        "8": ["t","u","v"],
        "9": ["w","x","y","z"],
    }

    def backtrack(conbination, nums):
        if len(nums) == 0:
            res.append(conbination)
        else:
            for letter in number_map[nums[0]]:
                backtrack(conbination + letter, nums[1:])

    res = []
    backtrack("", nums)
    return  res


if __name__ == '__main__':
    print(letterCombinations("23"))


