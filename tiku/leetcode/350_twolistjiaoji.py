def run(list1, list2):
    """
    思路：
    统计每个列表的元素出现的次数，汇聚到各自哈希表中，然后用key取交集，然后取出每个key在字典中出现的最小值（因为交集不会是多的一方，按照少的来计算）
    然后在构造返回的list
    """
    dict1 = {}
    for i in list1:
        if i not in dict1:
            dict1[i] = 1
        else:
            dict1[i] += 1
    dict2 = {}
    for i in list2:
        if i not in dict2:
            dict2[i] = 1
        else:
            dict2[i] += 1
    set_list = list(set(list(dict1.keys())) & set(list(dict2.keys())))
    res = []
    for j in set_list:
        num_count = min(dict1[j], dict2[j])
        for n in range(num_count):
            res.append(j)
    print(res)
    return res


if __name__ == '__main__':
    nums1 = [1, 2, 2, 1]
    nums2 = [2, 2]
    # return [2, 2]
    run(nums1, nums2)