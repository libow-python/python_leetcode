def square_set_zero(matrix):
    """
    要求原地算法
    matrix = [[1,1,1],[1,0,1],[1,1,1]]
    return = [1,0,1],[0,0,0],[1,0,1]
    """
    if not matrix:
        return
    row_len, col_len = len(matrix), len(matrix[0])
    log_list = []

    for r_i in range(row_len):
        for c_i in range(col_len):
            if matrix[r_i][c_i] == 0:
                log_list.append((r_i, c_i))
    print(log_list)

    row_list = list(set([i for i, _ in log_list]))
    col_list = list(set([i for _, i in log_list]))

    for r_i in range(row_len):
        for c_i in range(col_len):
            if r_i in row_list or c_i in col_list:
                matrix[r_i][c_i] = 0

    return matrix

if __name__ == '__main__':
    inp = [[1, 1, 1], [1, 0, 1], [1, 1, 1]]
    inp1 = [[0,1,2,0],[3,4,5,2],[1,3,1,5]]
    print(square_set_zero(inp1))