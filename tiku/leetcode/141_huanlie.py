class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


def has_cycle(head):
    # 快慢指针
    # if not head or not head.next:
    #     return False
    # left, right = head, head
    # while left and right:
    #     left = left.next
    #     if right.next:
    #         right = right.next.next
    #     else:
    #         return False
    #     if left == right:
    #         return True
    # return False
    # 映射
    d = []
    while head:
        if head.next in d:
            return True
        d.append(head)
        head = head.next
    return False



if __name__ == '__main__':
    node_head = ListNode(1)
    n2 = ListNode(2)
    n3 = ListNode(3)
    n4 = ListNode(4)
    node_head.next = n2
    node_head.next.next = n3
    node_head.next.next.next = n4
    node_head.next.next.next.next = None
    print(has_cycle(node_head))
