class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


res = []


def preorder_traversal(root):
    """二叉树先序 根左右"""
    # 传入空值
    # if not t:
    #     return []
    # res.append(t.val)
    # # 递归的退出条件
    # # 返回什么不影响，只要是该树只有一个值的时候的返回
    # if not t.left and not t.right:
    #     return res
    # if t.left:
    #     preorder_traversal(t.left)
    # if t.right:
    #     preorder_traversal(t.right)
    # return res

    # 简化递归
    # def di_gui(r):
    #     if not r:
    #         return
    #     result.append(r.val)
    #     di_gui(r.left)
    #     di_gui(r.right)
    #
    # result = []
    # di_gui(t)
    # return result

    # 迭代模拟递归 通常使用栈， 手动维护的栈模拟系统栈
    result = []
    if not root:
        return []
    stack = []
    stack.append(root)

    while stack:
        node = stack.pop()
        result.append(node.val)

        if node.right:
            stack.append(node.right)

        if node.left:
            stack.append(node.left)

    return result




if __name__ == '__main__':
    t_head = TreeNode(1)
    t_head.right = TreeNode(2)
    t_head.right.left = TreeNode(3)

    print(preorder_traversal(t_head))
