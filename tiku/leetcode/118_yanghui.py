def _generate(floor):
    result = [[] for _ in range(floor)]
    for i in range(floor):
        if i == 0:
            result[i].append(1)
        else:
            result[i].append(1)
            # 计算上一层的和列表
            inner = calculate_list(result[i-1])
            for j in inner:
                result[i].append(j)
            result[i].append(1)
    return result


def calculate_list(param_list):
    res = []
    l_s = len(param_list)
    if l_s <= 1:
        return []
    for ind in range(l_s-1):
        if ind + 1 >= l_s:
            break
        else:
            res.append(param_list[ind] + param_list[ind+1])
    return res


if __name__ == '__main__':
    f = 5
    print(_generate(f))
    # print(calculate_list([1, 3,3, 1]))