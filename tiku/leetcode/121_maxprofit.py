def run(prices):
    """
    动态规划：
    本题倒着看：
    只要知道当天以及以后的范围内的最大值，就可以
    """
    max_earning = 0
    lens = len(prices)
    result = 0
    for ind in range(lens-1, -1, -1):
        price = prices[ind]
        max_earning = max(price, max_earning)
        earn = max_earning - price
        if earn > 0:
            result = max(result, earn)
    return result


if __name__ == '__main__':
    p = [7, 1, 5, 3, 6, 4]
    # p = [7, 6, 4, 3, 1]
    print(run(p))
