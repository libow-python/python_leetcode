class TreeNode:

    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class TreeErgodic:

    def __init__(self):
        self.res = []

    def postorderTraversal(self, root):
        """
        二叉树后序遍历，递归
        """
        # if not root:
        #     return self.res
        #
        # if not root.left and not root.right:
        #     self.res.append(root.val)
        #     return self.res
        #
        # if root.left:
        #     self.postorderTraversal(root.left)
        #
        # if root.right:
        #     self.postorderTraversal(root.right)
        #
        # self.res.append(root.val)
        # return self.res
        # 简化的递归代码
        def di_gui(r):
            if not r:
                return
            di_gui(r.left)
            di_gui(r.right)
            res.append(r.val)

        res = []
        di_gui(root)
        return res


if __name__ == '__main__':
    t_h = TreeNode(1)
    t_h.left = None
    t_h.right = TreeNode(2)
    t_h.right.left = TreeNode(3)

    print(TreeErgodic().postorderTraversal(t_h))
