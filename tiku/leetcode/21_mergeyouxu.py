# 合并两个有序链表

class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


def mergeTwoLists(h_1, h_2):
    # 普通算法
    # res_l = None
    # cur = None
    # while h_1 or h_2:
    #     if not h_1:
    #         if isinstance(res_l, ListNode):
    #             res_l.next = ListNode(h_2.val)
    #             res_l = res_l.next
    #         else:
    #             res_l = ListNode(h_2.val)
    #             cur = res_l
    #         h_2 = h_2.next
    #         continue
    #     if not h_2:
    #         if isinstance(res_l, ListNode):
    #             res_l.next = ListNode(h_1.val)
    #             res_l = res_l.next
    #         else:
    #             res_l = ListNode(h_1.val)
    #             cur = res_l
    #         h_1 = h_1.next
    #         continue
    #     if h_1.val >= h_2.val:
    #         if isinstance(res_l, ListNode):
    #             res_l.next = ListNode(h_2.val)
    #             res_l = res_l.next
    #         else:
    #             res_l = ListNode(h_2.val)
    #             cur = res_l
    #         h_2 = h_2.next
    #     else:
    #         if isinstance(res_l, ListNode):
    #             res_l.next = ListNode(h_1.val)
    #             res_l = res_l.next
    #         else:
    #             res_l = ListNode(h_1.val)
    #             cur = res_l
    #         h_1 = h_1.next
    # return cur

    # 递归算法
    if not h_1:
        return h_2
    if not h_2:
        return h_1

    if h_1.val <= h_2.val:
        h_1.next = mergeTwoLists(h_1.next, h_2)
        return h_1
    else:
        h_2.next = mergeTwoLists(h_1, h_2.next)
        return h_2


if __name__ == '__main__':
    head_one = ListNode(1)
    head_one.next = ListNode(2)
    head_one.next.next = ListNode(4)

    head_t = ListNode(1)
    head_t.next = ListNode(3)
    head_t.next.next = ListNode(4)

    s = mergeTwoLists(head_one, head_t)
    print(s)
    print(s.val)
    print(s.next.val)

