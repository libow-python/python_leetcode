def run(ransomNote, magazine):
    # 判断连续相等
    # rans_len = len(ransomNote)
    # xunhuan_num = len(magazine) - rans_len + 1
    # if xunhuan_num <= 0:
    #     return False
    # result_list = []
    # for i in range(xunhuan_num):
    #     result_list.append(magazine[i: i+rans_len])
    # print(result_list)
    # if ransomNote in result_list:
    #     return True
    # return False

    # 非连续相等
    rans_count_d = {}
    for i in ransomNote:
        if i not in rans_count_d:
            rans_count_d[i] = 1
        else:
            rans_count_d[i] += 1

    maga_count_d = {}
    for i in magazine:
        if i not in maga_count_d:
            maga_count_d[i] = 1
        else:
            maga_count_d[i] += 1

    for i in rans_count_d.keys():
        if not maga_count_d.get(i) or rans_count_d[i] > maga_count_d.get(i):
            return False
    return True


if __name__ == '__main__':
    a = "a"
    b = "b"
    print(run(a, b))