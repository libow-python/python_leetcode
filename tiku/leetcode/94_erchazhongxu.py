class TreeNode:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class TwoTree:
    def __init__(self):
        self.res = []

    def inorderTraversal(self, root):
        """
        二叉树中序 左根右, 递归做法
        """
        # if not root:
        #     return self.res
        #
        # if not root.left and not root.right:
        #     # 因为中序要在递归退出的时候将当前节点添加到返回中
        #     self.res.append(root.val)
        #     return self.res
        #
        # # 先递归左子树
        # if root.left:
        #     self.inorderTraversal(root.left)
        #
        # self.res.append(root.val)
        #
        # if root.right:
        #     self.inorderTraversal(root.right)
        # return self.res

        # 简化递归
        # def di_gui(r):
        #     if not r:
        #         return
        #     di_gui(r.left)
        #     res.append(r.val)
        #     di_gui(r.right)
        #
        # res = []
        # di_gui(root)
        # return res

        # 非递归， stack模拟
        """
        1.同理创建一个 Stack，然后按 左 中 右的顺序输出节点。
        2. 尽可能的将这个节点的左子树压入 Stack，此时栈顶的元素是最左侧的元素，其目的是找到一个最小单位的子树(也就是最左侧的一个节点)，并且在寻找的过程中记录了来源，才能返回上层,同时在返回上层的时候已经处理完毕左子树了
        3. 当处理完最小单位的子树时，返回到上层处理了中间节点。（如果把整个左中右的遍历都理解成子树的话，就是处理完 左子树->中间(就是一个节点)->右子树)
        4. 如果有右节点，其也要进行中序遍历
        """
        res = []
        if not root:
            return []
        cur = root
        stack = []
        stack_vale = []
        while stack or cur:
            while cur:
                stack.append(cur)
                stack_vale.append(cur.val)
                cur = cur.left
            node = stack.pop()
            v = stack_vale.pop()
            res.append(node.val)
            if node.right:
                cur = node.right
        return res


if __name__ == '__main__':
    t_h = TreeNode(1)
    t_h.left = TreeNode(2)
    t_h.right = TreeNode(3)
    t_h.left.left = TreeNode(4)
    t_h.left.right = TreeNode(5)
    t_h.right.left = TreeNode(6)
    t_h.right.right = TreeNode(7)

    print(TwoTree().inorderTraversal(t_h))
