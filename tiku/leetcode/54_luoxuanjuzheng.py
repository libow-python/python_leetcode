def run(juzhen):
    # 定义四个方向 [右下左上]
    directions = [(1, 0), (0, 1), (-1, 0), (0, -1)]
    stack = []
    res = []
    count = 0  # 记录方向的标志
    cur = (0, 0)
    if not juzhen:
        return []
    x_max = len(juzhen[0])
    y_max = len(juzhen)
    if y_max == 0:
        return []
    if x_max == 0:
        return []
    total_num = x_max * y_max
    while True:
        direct = directions[count % 4]
        cur_x, cur_y = cur
        next_x, next_y = direct[0] + cur_x, direct[1] + cur_y
        if len(res) != total_num - 1 and (next_x < 0 or next_x > x_max - 1 or next_y < 0 or next_y > y_max - 1 or (next_x, next_y) in stack):
            # 最后一个值一定进入了环内所以不用考虑下一个值
            count += 1
        else:
            stack.append(cur)
            res.append(juzhen[cur_y][cur_x])
            cur = (next_x, next_y)
        if len(res) == total_num:
            break
    print(res)

def run1(matrix):
    res = []
    while matrix:
        res += matrix.pop(0)
        matrix = list(zip(*matrix))[::-1]
    return res

if __name__ == '__main__':
    juzhen = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
    # juzhen = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]]
    run1(juzhen)
