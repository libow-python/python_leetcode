def matrixReshape(shapes, r, c):
    """
    这个 % ， // 也能算
    """
    make_shape = []
    for s in shapes:
        make_shape += s
    if len(make_shape) != r * c:
        return shapes
    result = [[] for _ in range(r)]
    count = 0
    for num in make_shape:
        if len(result[count]) >= c:
            count += 1
        result[count].append(num)
    return result


if __name__ == '__main__':
    ls = [[1, 2]]
    r_p = 1
    c_p = 1
    print(matrixReshape(ls, r_p, c_p))
