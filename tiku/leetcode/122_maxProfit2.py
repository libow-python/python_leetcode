def run(prices):
    pass
    """
    贪心算法？只要我有盈利我就卖
    """
    res = 0
    for ind in range(len(prices) - 1):
        if prices[ind + 1] - prices[ind] > 0:
            res += prices[ind + 1] - prices[ind]
    print(res)
    return res


if __name__ == '__main__':
    # p = [7, 1, 5, 3, 6, 4]  # 7
    p = [7,6,4,3,1]  # 7
    print(run(p))
