from collections import Counter


def run(s, t):
    """
    1.
    标签：哈希映射
    首先判断两个字符串长度是否相等，不相等则直接返回 false
    若相等，则初始化 26 个字母哈希表，遍历字符串 s 和 t
    s 负责在对应位置增加，t 负责在对应位置减少
    如果哈希表的值都为 0，则二者是字母异位词
    2.
    Counter
    3. 单类型字符串可用排序
    """
    return Counter(s) == Counter(t)


if __name__ == '__main__':
    run()
