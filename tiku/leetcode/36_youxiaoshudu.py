def run(board):
    # 方法一， 两个指针切9宫
    # row_len = len(board)
    # col_len = len(board[0])
    # r = []
    # # 对比9宫格
    # for num in range(0, row_len, 3):
    #     for num_1 in range(0, col_len, 3):
    #         # 接下来进行拼接
    #         res = []
    #         for i in range(num, num+3):
    #             res.extend(board[i][num_1:num_1+3])
    #         res = [i for i in map(lambda a: a if a != "." else None, res) if i]
    #         if len(list(set(res))) != len(res):
    #             return False
    #         r.append(res)
    # # 对比每一行
    # for i in board:
    #     i = [j for j in map(lambda a: a if a != "." else None, i) if j]
    #     if len(list(set(i))) != len(i):
    #         return False
    # # 对比每一列
    # r = list(zip(*board))
    # for i in r:
    #     i = [j for j in map(lambda a: a if a != "." else None, i) if j]
    #     if len(list(set(i))) != len(i):
    #         return False
    # return True

    # 方法二： 数组和字典去映射
    """
    思路：构造3个结构去统计每个方块里的元素统计
    行： 构造一个二元数组，row_list = [[0-8],[0-8],[0-8] ....] 因为数独的元素是1 - 9，那么我统计每个元素出现的次数即可，元素出现的次数是row_list[行数][index] index : 用二维数组的下标当作元素(i - 1)
    列：同样是构造一个二元数组 col_list = [[0-8],[0-8],[0-8] ....], 原理同上
    九宫格：构造3元数组，subboxs = [[[0-8], [0-8], [0-8]], [[0-8], [0-8], [0-8]], [[0-8], [0-8], [0-8]]]
    原理类似行和列 9 * 9数独可分为 9个九宫格，也就是行3，列3，subboxs每个元素代表一个九宫格，每个元素同样是 [0-8], 这样的结构去统计九宫格的数量
    在board中的元素的下标，p = [i/3][j/3] 得到的就是 这个元素在九宫格的宫位，然后根据本身数字来确定他所在的位置进行统计
    """
    if not board:
        return False
    row_num, col_num = len(board), len(board[0])
    row_list = [[0] * 9 for _ in range(9)]
    col_list = [[0] * 9 for _ in range(9)]
    subboxs = [[[0] * 9 for _ in range(3)] for _ in range(3)]

    for row_index in range(row_num):
        for col_index in range(col_num):
            if board[row_index][col_index] == ".":
                continue
            index = int(board[row_index][col_index]) - 1
            row_list[row_index][index] += 1
            col_list[col_index][index] += 1
            subboxs[int(row_index / 3)][int(col_index / 3)][index] += 1
            if row_list[row_index][index] > 1 or col_list[col_index][index] > 1 or subboxs[int(row_index / 3)][int(col_index / 3)][index] > 1:
                return False
    return True







if __name__ == '__main__':
    board = [["5", "3", ".", ".", "7", ".", ".", ".", "."]
        , ["6", ".", ".", "1", "9", "5", ".", ".", "."]
        , [".", "9", "8", ".", ".", ".", ".", "6", "."]
        , ["8", ".", ".", ".", "6", ".", ".", ".", "3"]
        , ["4", ".", ".", "8", ".", "3", ".", ".", "1"]
        , ["7", ".", ".", ".", "2", ".", ".", ".", "6"]
        , [".", "6", ".", ".", ".", ".", "2", "8", "."]
        , [".", ".", ".", "4", "1", "9", ".", ".", "5"]
        , [".", ".", ".", ".", "8", ".", ".", "7", "9"]]


    bo1 = [["8", "3", ".", ".", "7", ".", ".", ".", "."]
        , ["6", ".", ".", "1", "9", "5", ".", ".", "."]
        , [".", "9", "8", ".", ".", ".", ".", "6", "."]
        , ["8", ".", ".", ".", "6", ".", ".", ".", "3"]
        , ["4", ".", ".", "8", ".", "3", ".", ".", "1"]
        , ["7", ".", ".", ".", "2", ".", ".", ".", "6"]
        , [".", "6", ".", ".", ".", ".", "2", "8", "."]
        , [".", ".", ".", "4", "1", "9", ".", ".", "5"]
        , [".", ".", ".", ".", "8", ".", ".", "7", "9"]]
    # r = list(zip(*board))
    # print(r)
    #
    print(run(board))
