def run(nums, tgt):
    result = []
    record_dict = {}
    for ind, n in enumerate(nums):
        if tgt - n in record_dict:
            result = [record_dict[tgt-n], ind]
        else:
            record_dict[n] = ind
    print(result)
    return result


if __name__ == '__main__':
    num = [3, 2, 4]
    target = 6
    run(num, target)
