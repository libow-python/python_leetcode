# 最大岛屿子面积
# 注意看深度和广度探查的方式不同，而且停止探查也不同

grid = [[0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
        [0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0],
        [0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0]]

dirs = [
	lambda x, y: (x + 1, y),
	lambda x, y: (x - 1, y),
	lambda x, y: (x, y + 1),
	lambda x, y: (x, y - 1),
]

# 广度
def maxAreaOfIslandbfs(area, x, y):
	stack = []
	stack.append((x, y))
	area[x][y] = 2
	count = 1
	while stack:
		cur_node = stack[-1]
		flag = True
		for d in dirs:
			next_node = d(cur_node[0], cur_node[1])
			# 向四周探查
			# print(next_node[0],next_node[1])
			# 广度优先探查，只有一次也没探查到才pop
			print(next_node[0], "node_next0")
			a = next_node[0]
			b = next_node[1]
			c = len(area)
			hh = len(area[0])
			print(next_node[1], "node_next1")
			print(len(area), "len(area)")
			print(len(area[0]), "len(area[0])")
			if next_node[0] >= 0 and next_node[1] >= 0 and next_node[0] < len(area) and next_node[1] < len(area[0]) and \
				area[next_node[0]][next_node[1]] == 1:
				stack.append(next_node)
				area[next_node[0]][next_node[1]] = 2
				count += 1
				flag = False
		if flag:
			stack.pop()
	return count


# 深度
def maxAreaOfIslanddfs(area, x, y):
	stack = []
	stack.append((x,y))
	area[x][y] = 2
	count = 1
	while stack:
		cur_node = stack[-1]
		for d in dirs:
			next_node = d(cur_node[0], cur_node[1])
			if next_node[0] >= 0 and next_node[0] < len(area) and next_node[1] >=0 and next_node[1] < len(area[0]) and area[next_node[0]][next_node[1]] == 1:
				stack.append(next_node)
				area[next_node[0]][next_node[1]] = 2
				count += 1
				break
		else:
			area[cur_node[0]][cur_node[1]] = 2
			stack.pop()
	return count



def first(grid):
	if not grid: return 0
	cols = len(grid)
	rows = len(grid[0])
	lst = [0]
	for i in range(cols):
		for j in range(rows):
			if grid[i][j] == 1:
				# res = maxAreaOfIslanddfs(grid, i, j)
				res = maxAreaOfIslandbfs(grid, i, j)
				lst.append(res)
	print(lst, "lst")
	return max(lst)


if __name__ == '__main__':
	# gridq = [[0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
	#          [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
	#          [0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
	#          [0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0],
	#          [0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0],
	#          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
	#          [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
	#          [0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0]]
	#
	# s = [[0, 0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0],
	#      [0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0],
	#      [0, 2, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0],
	#      [0, 2, 0, 0, 2, 2, 0, 0, 2, 0, 2, 0, 0],
	#      [0, 2, 0, 0, 2, 2, 0, 0, 2, 2, 2, 0, 0],
	#      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0],
	#      [0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0],
	#      [0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0]]

	gg = [[1,1]]

	print(first(gg))
	print(gg)
	# print(gridq)
	# x = 0
	# y = 1
	# a = len(gg)
	# b = len(gg[0])
	# if x >= 0 and x < len(gg) and y >= 0 and y < len(gg[0]) and gg[x][y] == 1:
	# 	print(11111111111111)
	# print(a,b,gg[x][y])

